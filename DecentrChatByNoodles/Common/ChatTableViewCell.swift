//
//  ChatTableViewCell.swift
//  SmartOffice
//
//  Created by Maria Bolshakova on 07.10.2021.
//

import SnapKit
import UIKit

fileprivate enum Const {
    enum Font {
        static let name: UIFont = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
    }
    
    enum Avatar {
        static let size: CGFloat = 48.0
        static let leading: CGFloat = 24.0
        static let top: CGFloat = 25.0
        static let cornerRadius: CGFloat = 24.0
    }
    enum Name {
        static let leading: CGFloat = 16.0
    }
}

final class ChatTableViewCell: UITableViewCell {
    // MARK: Private Variables
    private let avatarImage: UIImageView = {
        let avatarImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 48, height: 48))
        avatarImage.contentMode = .scaleAspectFill
        avatarImage.layer.cornerRadius = Const.Avatar.cornerRadius
        avatarImage.clipsToBounds = true
        return avatarImage
    }()
    
    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.numberOfLines = 1
        nameLabel.textColor = UIColor.white
        nameLabel.font = Const.Font.name
        return nameLabel
    }()
    
    
    // MARK: Initializers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.addSubview(avatarImage)
        contentView.addSubview(nameLabel)
        contentView.backgroundColor = .clear
        setupConstraints()
        configureAvatar()
    }
}

// MARK: - Private Functions
extension ChatTableViewCell {
    private func setupConstraints() {
        setupAvatarConstraints()
        setupNameConstraints()
    }
    
    private func setupAvatarConstraints() {
        avatarImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(Const.Avatar.leading)
            make.size.equalTo(Const.Avatar.size)
        }
    }
    
    private func setupNameConstraints() {
        nameLabel.snp.makeConstraints { make in
            make.centerY.equalTo(avatarImage.snp.centerY)
            make.leading.equalTo(avatarImage.snp.trailing).offset(Const.Name.leading)
            make.trailing.equalTo(nameLabel.snp.trailing)
        }
    }
    
    private func configureAvatar() {
        avatarImage.image = AvatarList.avatars.randomElement()
    }
    
    func configureName(with name: String?) {
        nameLabel.text = name
    }
}

