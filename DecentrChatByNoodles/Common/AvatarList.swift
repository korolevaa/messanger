//
//  AvatarList.swift
//  DecentrChatByNoodles
//
//  Created by Maria Bolshakova on 07.10.2021.
//

import Foundation
import UIKit

enum AvatarList {
    static let avatar1: UIImage = {
        let icon = UIImage(named: "heartsEyes")
        return icon!
    }()
    
    static let avatar2: UIImage = {
        let icon = UIImage(named: "rainbowEyes")
        return icon!
    }()
    
    static let avatar3: UIImage = {
        let icon = UIImage(named: "starsEyes")
        return icon!
    }()
    
    static let avatar4: UIImage = {
        let icon = UIImage(named: "XXeyes")
        return icon!
    }()
    
    static let avatar5: UIImage = {
        let icon = UIImage(named: "tongue")
        return icon!
    }()
    
    static let avatar6: UIImage = {
        let icon = UIImage(named: "wink")
        return icon!
    }()
    
    static let avatars: [UIImage] = [avatar1, avatar2, avatar3, avatar4, avatar5, avatar6]
}
