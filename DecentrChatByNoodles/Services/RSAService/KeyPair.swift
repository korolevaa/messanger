//
//  KeyPair.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


class KeyPair {
    let publicKey: Data
    let privateKey: Data
    init(publicKey: Data, privateKey: Data){
        self.publicKey = publicKey
        self.privateKey = privateKey
    }
}
