//
//  RSAContract.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


protocol RSACryptable {
   
    /**
     Создает пару публичный/приватный ключ
     */
    func generateKeyPair() -> KeyPair?
    /**
     Шифрование данных публичным ключем.
     @discussion Шифрование данных малых объемов, которые целесообразно хранит в памяти.
     */
    func decrypt(fromCipherData cipherData: Data, usingRSA rsa: RSA) -> Data?
    func encrypt(fromData data: Data, usingRSA rsa: RSA) -> Data?
    func createSignature(data: Data, usingKey key: Data) -> Data?
}
