//
//  RSA.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


struct RSA {
    enum Key {
        case `private`(Data), `public`(Data)
    }
    
    enum KeyAlgorithm {
        case rsaEncryptionPKCS1 // kSecKeyAlgorithmRSAEncryptionPKCS1
        case rsaSignatureDigestPKCS1v15SHA256// rsaSignatureDigestPKCS1v15SHA256
        case rsaEncryptionRaw //
    }
    
    let key: RSA.Key
    let algorithm: KeyAlgorithm = .rsaEncryptionPKCS1
}
