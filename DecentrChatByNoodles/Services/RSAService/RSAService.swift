//
//  RSAService.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import CommonCrypto
import Security
import XCTest


extension RSAService {
    enum Result<T> {
        case failure(Error), success(T)
    }
    
    enum RSAError: Error {
        case generateRandomData(OSStatus)
        case generateSymetricKey
    }
    
}


class RSAService {
    static let `default` = RSAService()
}


// MARK: - RSACryptable
extension RSAService: RSACryptable {
    func generateKeyPair() -> KeyPair? {
        let attributes: [String: Any] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeySizeInBits as String: 2048,
            kSecPrivateKeyAttrs as String: [
                kSecAttrIsPermanent as String: false,
                kSecAttrAccessible as String: kSecAttrAccessibleAfterFirstUnlock,
            ],
            kSecPublicKeyAttrs as String: [
                kSecAttrIsPermanent as String: false,
                kSecAttrAccessible as String: kSecAttrAccessibleAfterFirstUnlock,
            ],
        ]
        
        guard let privateSecKey = SecKeyCreateRandomKey(attributes as CFDictionary, nil),
              let publicSecKey = SecKeyCopyPublicKey(privateSecKey),
              let publicKeyData = SecKeyCopyExternalRepresentation(publicSecKey, nil),
              let privateKeyData = SecKeyCopyExternalRepresentation(privateSecKey, nil)
        else {
            return nil
        }
        
        return KeyPair(
            publicKey: publicKeyData as Data,
            privateKey: privateKeyData as Data
        )
    }
    
    func createSignature(data: Data, usingKey key: Data) -> Data? {
        let attributes: [String : CFString] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeyClass as String: kSecAttrKeyClassPrivate //SecKeyAlgorithm.rsaSignatureMessagePKCS1v15SHA256
        ]
        
        let key = SecKeyCreateWithData(
            key as CFData,
            attributes as CFDictionary,
            nil
        )
        
        guard let key = key else {
            return nil
        }
        
        let signature = SecKeyCreateSignature(
            key, .rsaSignatureMessagePKCS1v15SHA256,
            data as CFData, nil)
        
        return signature as Data?
    }
    
    func encrypt(fromData data: Data, usingRSA rsa: RSA) -> Data? {
        let key = SecKeyCreateWithData(
            rsa.keyData as CFData,
            rsa.attributes as CFDictionary,
            nil
        )

        guard let key = key else {
            return nil
        }
        
        guard SecKeyIsAlgorithmSupported(key, .decrypt, rsa.secAlgorithm) else {
            return nil
        }
        
        let cipherData = SecKeyCreateEncryptedData(
            key, rsa.secAlgorithm,
            data as CFData,
            nil
        )
        
        return cipherData as Data?
    }
    
    func decrypt(fromCipherData cipherData: Data, usingRSA rsa: RSA) -> Data? {
        let key = SecKeyCreateWithData(
            rsa.keyData as CFData,
            rsa.attributes as CFDictionary,
            nil
        )

        guard let key = key else {
            return nil
        }
        
        guard SecKeyIsAlgorithmSupported(key, .decrypt, rsa.secAlgorithm) else {
            return nil
        }
        
        let data = SecKeyCreateDecryptedData(
            key, rsa.secAlgorithm,
            cipherData as CFData,
            nil
        )
        
        return data as Data?
    }
}

fileprivate extension RSA {
    var secAttrValueClass: CFString {
        switch key {
        case .private:
            return kSecAttrKeyClassPrivate
        case .public:
            return kSecAttrKeyClassPublic
        }
    }
    
    var secAlgorithm: SecKeyAlgorithm {
        switch algorithm {
        case .rsaEncryptionPKCS1: // kSecKeyAlgorithmRSAEncryptionPKCS1
            return .rsaEncryptionPKCS1        
        case .rsaSignatureDigestPKCS1v15SHA256:
            return .rsaSignatureDigestPKCS1v15SHA256
        case .rsaEncryptionRaw:
            return .rsaEncryptionRaw
        }
    }
    
    var keyData: Data {
        switch key {
        case let .private(data):
            return data
        case let .public(data):
            return data
        }
    }
    
    var attributes: [String : CFString] {
        return [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeyClass as String: secAttrValueClass
        ]
    }
}
