//
//  AuthService.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import Foundation
import CertificateSDK

/// Набор констант, используемых для конфигурации проекта`
enum Config {
    /// Константы использующиеся для фреймворка PKI SDK
    struct PKISDK {
        let serverURL: URL = URL(string: "https://pki.sberbank.ru")!

        /// Идентификатор приложения необходимый для авторизации
        let appID: String = "9a294848-4e88-4cb8-8cd5-ab2600c39b52"

        /// URL схема приложения для обработки ответа сервера PKI
        let clientAppScheme: String = "smartoffice"
    }
}

private extension AuthService {
    struct User: Codable {
        let id: String
        let createdAt: Double
        let emails: [String]
        let nick: String
        let publicKey: Data
        let addres: String
    }
}

class AuthService: NSObject {
	static let `default`: AuthService = {
		return AuthService()
	}()
	
    private let profileService = ProfileService.default
    private let secureService = SecureService.default
    
    private let pkiCertificateManager = SBPKICertificateManager(serverURL: Config.PKISDK().serverURL)
    
    //Public
    var didSaveUser: ((DTO.User) -> Void)?
    
    func getPKIAuthorizationVC() -> SBPKIAuthorizationViewController {
        let authVC = SBPKIAuthorizationViewController(serverURL: Config.PKISDK().serverURL,
                                                      appID: Config.PKISDK().appID,
                                                      clientAppScheme: Config.PKISDK().clientAppScheme)
        
        authVC.delegate = self
        return authVC
    }
}

extension AuthService: SBPKIAuthorizationViewControllerDelegate {
    func authorizationViewController(_ controller: SBPKIAuthorizationViewController, didReceiveToken token: String) {
        print("didReceiveToken From PKIVC 🔥")
        didReceive(token: token) { [weak self] error in
            if let error = error {
                print("\(error)🍒") //need to show alert
            } else {
                if let user = self?.profileService.owner {
                    self?.didSaveUser?(user)
                }
            }
        }
    }
    
    func authorizationViewController(_ controller: SBPKIAuthorizationViewController, didReceiveError error: Error) {
        print("\(error) 🥩")
    }
}


extension AuthService {
    func didReceive(token: String, with completion: @escaping ((Error?) -> Void)) {
            self.pkiCertificateManager.requestCertificate(withToken: token, type: SBPKICertificateType.user, completionBlock: { [weak self] certificates, errors in
                guard let certificate = certificates?.first else {
                    print("No certificates received from PKI ❌ ")
                    return completion(errors?.first)
                }
                defer {
                    completion(nil)
                }
                guard let self = self else {
                    return
                }
                self.getCertInfoForUser(certificate: certificate)
            })
    }
    
    func getCertInfoForUser(certificate: SBPKICertificate) {
        var certificateSecIdentityUnmanaged: Unmanaged<SecIdentity>?
        var certificateSecCertificateUnmanaged: Unmanaged<SecCertificate>?
        
        certificate.getIdentity(
            &certificateSecIdentityUnmanaged,
            certificate: &certificateSecCertificateUnmanaged
        )
                
        guard   let secCertificate: SecCertificate = certificateSecCertificateUnmanaged?.takeRetainedValue(),
                let secIdentity: SecIdentity = certificateSecIdentityUnmanaged?.takeRetainedValue()
        else {
            print("No CertificateSecCertificate 🍎")
            return
        }
        
        do {
            let derCertificateCFData = SecCertificateCopyData(secCertificate)
            let x509 = try X509Certificate(data: derCertificateCFData as Data)

            guard
                let mail = x509.subject(dn: .email),
                let commonName = x509.subject(dn: .commonName)
            else {
               print("Unable to save Certificate")
                return
            }
            
            var privateKey: SecKey?
            _ = SecIdentityCopyPrivateKey(secIdentity, &privateKey)
            let publicKey = SecKeyCopyPublicKey(privateKey!)
            
            let keyPair = KeyPair(
                publicKey: SecKeyCopyExternalRepresentation(publicKey!, nil)! as Data,
                privateKey: SecKeyCopyExternalRepresentation(privateKey!, nil)! as Data
            )
            
            let user = User(
                id: commonName,
                createdAt: Date().timeIntervalSince1970,
                emails: [mail],
                nick: commonName,
                publicKey: keyPair.publicKey,
                addres: "address/test")
                        
            self.secureService.userKeyPair = keyPair
            self.profileService.derCertificate = derCertificateCFData as Data
            self.profileService.owner = DTO.User(
                user: user,
                signature: secureService.signature(user)!
            )
        } catch {
            print("DER certificate cannot ASN1 decoder")
        }
    }
	
	func reacreateUser(addres: String) {
		let user = User(
			id: self.profileService.owner!.id,
			createdAt: Date().timeIntervalSince1970,
			emails: self.profileService.owner!.emails,
			nick: self.profileService.owner!.id,
			publicKey: self.profileService.owner!.publicKey,
			addres: addres)
		self.profileService.owner = DTO.User(
			user: user,
			signature: secureService.signature(user)!
		)
	}
}


// MARK: - DTO.User
fileprivate extension DTO.User {
    init(user: AuthService.User, signature: Data) {
        self.init(
            id: user.id,
            createdAt: user.createdAt,
            emails: user.emails,
            nick: user.nick,
            publicKey: user.publicKey,
            addres: user.addres,
            signature: signature
        )
    }
}
