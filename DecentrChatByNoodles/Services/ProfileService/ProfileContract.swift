//
//  ProfileContract.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


protocol ProfileInformable {
    var owner: DTO.User? { get set }
    var derCertificate: Data? { get set }
}
