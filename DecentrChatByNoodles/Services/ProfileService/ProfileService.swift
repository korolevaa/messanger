//
//  ProfileService.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import KeychainAccess


private let usDefOwnerKey = "com.bittalk.profileservice.owner"
private let usDefCertKey = "com.bittalk.profileservice.dercertificate"

class ProfileService {
    static let `default` = ProfileService()
    private let usDef = UserDefaults.standard
    private let secureService = SecureService.default
    private let keychain = Keychain(service: "com.bittalk.profileservice.keychain")
}


extension ProfileService: ProfileInformable {
    var owner: DTO.User? {
        get {
            return try? usDef.getObjectOwn(
                forKey: usDefOwnerKey,
                castTo: DTO.User.self
            )
        }
        set {
            let data = newValue
            try? usDef.setObjectOwn(newValue, forKey: usDefOwnerKey)
        }
    }
    
    var derCertificate: Data? {
        get {
            return try? keychain.getData(usDefCertKey)
        }
        set {
            if let data = newValue {
                try? keychain.set(data, key: usDefCertKey)
            } else {
                try? keychain.remove(usDefCertKey)
            }
        }
    }
}
