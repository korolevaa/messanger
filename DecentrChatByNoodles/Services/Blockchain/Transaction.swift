//
//  Transaction.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


struct BlockTransaction: Codable, Equatable {
    /// Порядковый номер транзакции он же айдишник
    let id: UInt64
    
    /// Создатель транзакции
    let owner: String
    
    /// Тип даных
    let kind: Int
    
    /// Данные
    let data: Data
    
    /// Подпись транзакции
    let signature: Data
}

