//
//  Blockchain.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import SwiftUI


class Blockchain: Codable {
	
	private let _bloackchainWasMined: Runner = Runner()
	var wasMined: (()->Void)? {
		set {
			self._bloackchainWasMined.wasMined = newValue
		}
		get {
			return self._bloackchainWasMined.wasMined
		}
	}
	
    // начальный пустой список для хранения транзакций
    private var currentTransactions: [BlockTransaction] = []

    // начальный пустой список для хранения вашей цепочки блоков
    private(set) var chain: [Block] = []

    init() {
        // Создайте блок генезиса
        createBlock(previousHash: "1".data(using: .utf8))
    }
    
    @discardableResult
    func mine() -> Block {
        let lastHash = lastBlock.hash()
		let block = createBlock(previousHash: lastHash)
		self._bloackchainWasMined.run()
        return block
    }
    
    // Создает новый Блок и добавляет его в цепочку
    @discardableResult
    func createBlock(previousHash: Data? = nil) -> Block {
        let prevHash: Data
        if let previousHash = previousHash {
            prevHash = previousHash
        } else {
            // Хеш предыдущего блока
            prevHash = lastBlock.hash()
        }
        let block = Block(index: chain.count+1,
                          timestamp: Date().timeIntervalSince1970,
                          transactions: currentTransactions,
                          previousHash: prevHash)
        
        // Сбросить текущий список транзакций
        currentTransactions = []
        chain.append(block)
        return block
    }

    // Добавляет новую транзакцию в список транзакций
    @discardableResult
    func appendTransaction(_ transaction: BlockTransaction) -> Int {
        currentTransactions.append(transaction)
        return lastBlock.index + 1
    }
    
    // Возвращает последний блок в цепочке
    var lastBlock: Block {
        guard let last = chain.last else {
            fatalError("The chain should have at least one block as a genesis.")
        }
        return last
    }
    
    var lastTransaction: BlockTransaction? {
        let transactions = currentTransactions.isEmpty
            ? lastBlock.transactions
            : currentTransactions
        
        return transactions.last
    }
    
    // Простой алгоритм доказательства работы:
    // - Найдите число p 'такое, что hash (pp') содержит четыре ведущих нуля, где p - предыдущий p '
    // - p - предыдущее доказательство, а p '- новое доказательство
    class func proofOfWork(lastProof: Int) -> Int {
        var proof: Int = 0
        while !validProof(lastProof: lastProof, proof: proof) {
            proof += 1
        }
        return proof
    }
    
    // Подтверждает доказательство:
    // - Содержит ли хэш (last_proof, proof) 4 ведущих нуля?
    class func validProof(lastProof: Int, proof: Int) -> Bool {
        guard let guess = String("\(lastProof)\(proof)").data(using: .utf8) else {
            fatalError()
        }
        let guess_hash = guess.sha256().hexDigest()
        return guess_hash.prefix(4) == "0000"
    }
	
	func coverBlockchain(with type:BlockchainType, identifier: DTO.ID?) -> TransmitCover? {
		if let blockchainData: Data = try? JSONEncoder().encode(self) {
			let blockchain = TransmitCover(data: blockchainData, type: type, signature: Data(), identifier: identifier)
			return blockchain
		}
		return nil
	}
}

extension Blockchain {
	func merge(blockchain: Blockchain) {
		var resBlockchain: Blockchain = Blockchain()
		var errorIndex: Int?
		for i in (0...max(blockchain.chain.count, self.chain.count)) {
			let extBlock = blockchain.chain.sf(i)
			let inBlock = self.chain.sf(i)
			guard extBlock == inBlock else {
				errorIndex = i
				break
			}
		}
		
		func createAndAdd(block: Block, to blockchain: Blockchain) {
			let newBlock = Block(index: blockchain.lastBlock.index + 1, timestamp: block.timestamp, transactions: block.transactions, previousHash: blockchain.lastBlock.hash())
			blockchain.chain.append(newBlock)
		}
		
		if let errorIndex = errorIndex {
			resBlockchain.chain.append(contentsOf:self.chain.prefix(upTo: errorIndex))
			for i in (errorIndex...max(blockchain.chain.count, self.chain.count)) {
				if let extBlock: Block = blockchain.chain.sf(i) {
					createAndAdd(block: extBlock, to: resBlockchain)
				}
				if let inBlock: Block = self.chain.sf(i) {
					createAndAdd(block: inBlock, to: resBlockchain)
				}
				
			}
		} else {
			resBlockchain.chain.append(contentsOf: self.chain)
		}
		self.chain.removeAll()
		self.chain = resBlockchain.chain
	}
}

extension Blockchain {
	
	static func genesisBlockhain() -> Blockchain {
		var blockchain = Blockchain.init()
		var transactions: [BlockTransaction] = []
		let user = DTO.User.init(
			id: "w",
			createdAt: 0,
			emails: ["Stive@sber.com"],
			nick: "Stiv Jobs",
			publicKey: Data(),
			addres: "http://wm2mpwoqvlpdwnip2tc54ueim46lo2hwnsyvagggak65wgh7v4w2llid.onion/",
			signature: Data()
		)
		let user2 = DTO.User.init(
			id: "e",
			createdAt: 0,
			emails: ["NotStive@sber.com"],
			nick: "Not Stiv Jobs",
			publicKey: Data(),
			addres: "http://ehbj26t5e357k55eo3ozh4lkzw2m5p5zcre3p5hnpgvksbmfhbxokxid.onion/",
			signature: Data()
		)
		
		let userData2: Data = (try? JSONEncoder().encode(user2)) ?? Data()
		let userData: Data = (try? JSONEncoder().encode(user)) ?? Data()
		let trans: BlockTransaction = BlockTransaction(
			id: (blockchain.lastTransaction?.id ?? 0) + 1,
			owner: "🐶",
			kind: DTO.Kind.contact.rawValue,
			data: userData,
			signature: Data()
		)
		blockchain.appendTransaction(trans)
		let trans2: BlockTransaction = BlockTransaction(
			id: (blockchain.lastTransaction?.id ?? 0) + 1,
			owner: "🐶!",
			kind: DTO.Kind.contact.rawValue,
			data: userData2,
			signature: Data()
		)
		blockchain.appendTransaction(trans2)
		blockchain.mine()
		return blockchain
	}
}

extension Array where Element == Block {
	func sf(_ index: Int) -> Element? {
		if self.count > index {
			return self[index]
		}
		return nil
	}
}

class Runner: Codable {
	var wasMined: (()->Void)?
	func run() {
		self.wasMined?()
	}
	init() {}
	required init(from decoder:Decoder) throws {}
	func encode(to encoder: Encoder) throws {}
}
