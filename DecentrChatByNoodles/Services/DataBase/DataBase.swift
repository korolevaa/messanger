//
//  DataBase.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import SwiftUI

private var path: URL {
    return FileManager.default
        .urls(for: .documentDirectory,in: .userDomainMask)[0]
        .appendingPathComponent("DB_4")
}

enum BlockchainType: String, Codable {
	case global = "global"
	case rooms = "rooms"
	case pantry = "pantry"
}

class DataBase: Codable {
    static let `default`: DataBase = {
        if  let archive = try? Data(contentsOf: path),
            let base = try? JSONDecoder().decode(DataBase.self, from: archive) {
            return base
        }
        return DataBase(
            globalBlockchain: .genesisBlockhain(),
            inviteRooms: [:],
            pantryRooms: [:])
    }()
    
    deinit {
        let archive = try? JSONEncoder().encode(self)
        try? archive?.write(to: path)
    }
    
    /// Глобальный блокчейн с информацией о пользователях и чатов
    let globalBlockchain: Blockchain
    
    /// Блокчейны чатов в которых состоит пользователь
    var inviteRooms: [DTO.ID: Blockchain]
    
    /// Кладовка, блокчейны чатов в которых пользователь не состоит, но по запросу может передать.
    var pantryRooms: [DTO.ID: Blockchain]
    
    init(globalBlockchain: Blockchain,
         inviteRooms: [DTO.ID: Blockchain],
         pantryRooms: [DTO.ID: Blockchain]) {
        self.globalBlockchain = globalBlockchain
        self.inviteRooms = inviteRooms
        self.pantryRooms = pantryRooms
    }
	
	func setupTorHandlers() {
		TorManager.shared.responseHandler = { request in
			if let trans = TransmitCover.unpack(request) {
				self.tryUpdateBlockchain(transmitCover: trans)
			}
			return ["Test":"Test"]
		}
		self.globalBlockchain.wasMined = { [weak self] in
			self?.allContacts().forEach({ user in
				if let userAddresURL = URL(string: user.addres) {
					if let params: Data = self?.globalBlockchain.coverBlockchain(with: .global, identifier: nil)?.pack {
						TorManager.shared.request(from: userAddresURL, postParams: params) { _ in
								
						}
					}
				}
			})
		}
		self.inviteRooms.forEach { (key: DTO.ID, value: Blockchain) in
			value.wasMined = { [weak self] in
				if let room = self?.room(withID: key) {
					room.members.forEach({ user in
						if let userAddresURL = URL(string: user.addres) {
							if let params: Data = value.coverBlockchain(with: .rooms, identifier: key)?.pack {
								TorManager.shared.request(from: userAddresURL, postParams: params) { _ in
								  
								}
							}
						}
					})
				}
			}
		}
	}
	
}


extension DataBase {
    func safeContext() {
        let archive = try? JSONEncoder().encode(self)
        try? archive?.write(to: path)
        DispatchQueue.global().asyncAfter(deadline: .now() + 1) {
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: DTO.modifyDataBaseNotificationKey, object: self)
            }
        }
    }
    
    func contact(forUserWithID userID: String) -> DTO.User? {
        var previousUser: DTO.User?
        for block in globalBlockchain.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let room = try? JSONDecoder().decode(DTO.Room.self, from: transaction.data)
                {
                    for user in room.members {
                        if user.id == userID {
                            if let _previousUser = previousUser {
                                if _previousUser.createdAt < user.createdAt {
                                    previousUser = user
                                }
                            } else {
                                previousUser = user
                            }
                            break
                        }
                    }
                }
            }
        }
        return previousUser
    }
    
    func contacts(forSearchQuery query: String) -> [DTO.User] {
        var users: [DTO.User] = []
        for block in globalBlockchain.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let room = try? JSONDecoder().decode(DTO.Room.self, from: transaction.data)
                {
                    for user in room.members {
                        if user.nick.contains(query) {
                            if !users.contains(where: { $0.id == user.id }) {
                                users.append(user)
                            }
                            continue
                        }
                            
                        for email in user.emails {
                            if email.contains(query) {
                                if !users.contains(where: { $0.id == user.id }) {
                                    users.append(user)
                                }
                                continue
                            }
                        }
                    }
                }
            }
        }
        return users
    }
    
    func allContacts(forRoomWithID roomID: DTO.ID) -> [DTO.User] {
        guard let room = room(withID: roomID) else {
            return []
        }
        return room.members
    }
    
    func allContacts() -> [DTO.User] {
        var users: [DTO.User] = []
        for block in globalBlockchain.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let room = try? JSONDecoder().decode(DTO.Room.self, from: transaction.data)
                {
                    for user in room.members {
                        if !users.contains(where: { $0.id == user.id }) {
                            users.append(user)
                        }
                    }
                    continue
                }
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .contact,
                    let user = try? JSONDecoder().decode(DTO.User.self, from: transaction.data)
                {
                    if !users.contains(where: { $0.id == user.id }) {
                        users.append(user)
                    }
                }
            }
        }
        return users
    }
    
    func room(withID id: DTO.ID) -> DTO.Room? {
        var previousRoom: DTO.Room?
        for block in globalBlockchain.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let room = try? JSONDecoder().decode(DTO.Room.self, from: transaction.data),
                    room.base.id == id
                {
                    if let _previousRoom = previousRoom {
                        if _previousRoom.base.createdAt < room.base.createdAt {
                            previousRoom = room
                        }
                    } else {
                        previousRoom = room
                    }
                }
            }
        }
        return previousRoom
    }
    
    func rooms(forSearchQuery query: String) -> [DTO.Room] {
        var rooms: [DTO.Room] = []
        for block in globalBlockchain.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let room = try? JSONDecoder().decode(DTO.Room.self, from: transaction.data),
                    room.name.uppercased().contains(query.uppercased())
                {
                    rooms.append(room)
                }
            }
        }
        return rooms
    }
    
    func allRooms() -> [DTO.Room] {
        var rooms: [DTO.Room] = []
        for roomID in inviteRooms.keys {
            if let room = room(withID: roomID) {
                rooms.append(room)
            }
        }
        return rooms.sorted { lR, rR in
            return lR.base.createdAt > rR.base.createdAt
        }
    }
    
    func message(withID messageID: DTO.ID, inRoomWithID roomID: DTO.ID) -> DTO.Message? {
        guard let room = inviteRooms[roomID] else {
            return nil
        }
        
        var previousMessage: DTO.Message?
        for block in room.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .message,
                    let message = try? JSONDecoder().decode(DTO.Message.self, from: transaction.data),
                    message.base.id == messageID
                {
                    if let _previousMessage = previousMessage {
                        if _previousMessage.base.createdAt < message.base.createdAt {
                            previousMessage = message
                        }
                    } else {
                        previousMessage = message
                    }
                }
            }
        }
        return previousMessage
    }
    
    func messages(forSearchQuery query: String, inRoomWithID roomID: DTO.ID) -> [DTO.Message] {
        guard let room = inviteRooms[roomID] else {
            return []
        }
        
        var messages: [DTO.Message] = []
        for block in room.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .room,
                    let message = try? JSONDecoder().decode(DTO.Message.self, from: transaction.data),
                    message.sourceText.contains(query)
                {
                    messages.append(message)
                }
            }
        }
        return messages
    }
    
    func allMessages(forRoomWithID roomID: DTO.ID) -> [DTO.Message] {
        guard let room = inviteRooms[roomID] else {
            return []
        }
        var messages: [DTO.Message] = []
        for block in room.chain {
            for transaction in block.transactions {
                if  let kind =  DTO.Kind(rawValue: transaction.kind), kind == .message,
                    let message = try? JSONDecoder().decode(DTO.Message.self, from: transaction.data)
                {
                    messages.append(message)
                }
            }
        }
        return messages.sorted { lM, rM in
            return lM.base.createdAt < rM.base.createdAt
        }
    }
    
    func cipherRoomKey(forRoomWithID roomID: DTO.ID) -> Data? {
        return room(withID: roomID)?.keys[ProfileService.default.owner!.id]
    }
	
	func tryUpdateBlockchain(transmitCover: TransmitCover) {
		if let blockchain = try? JSONDecoder().decode(Blockchain.self, from: transmitCover.data) {
			switch transmitCover.type {
			case .global:
				print("Updte Global")
				self.globalBlockchain.merge(blockchain: blockchain)
				DataBase.default.safeContext()
			case .rooms:
				if let identifier = transmitCover.identifier {
					print("Updte Rooms \(identifier)")
					if self.inviteRooms[identifier] != nil {
						self.inviteRooms[identifier]?.merge(blockchain: blockchain)
					} else {
						self.inviteRooms[identifier] = blockchain
						self.setupTorHandlers()
					}
					DataBase.default.safeContext()
				}
			case .pantry:
				break
			}
		}
	}
}
