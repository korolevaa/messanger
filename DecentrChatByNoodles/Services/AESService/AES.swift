//
//  AES.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


class AES {
    enum Operation {
        case encrypt, decrypt
    }
    
    enum Algoritm {
        case AES128
        case AES
        case DES
        case DES3
        case CAST
        case RC4
        case RC2
        case Blowfish
    }
    
    enum Options {
        case PKCS7Padding, ECBMode
    }
    
    enum BlockSize {
        case AES128 //AES block size (currently, only 128-bit)
        case arbitrary(Int)
    }
    
    
    let key: SymmetricKey
    let iv: InitVector?
    let operation: Operation
    let algorithm: Algoritm
    let options: Options
    let blockSize: BlockSize
    
    init(symmetricKey: SymmetricKey,
         initVector: InitVector? = nil,
         operation: Operation = .encrypt,
         algorithm: Algoritm = .AES,
         options: Options = .PKCS7Padding,
         blockSize: BlockSize = .AES128)
    {
        self.key = symmetricKey
        self.iv = initVector
        self.operation = operation
        self.algorithm = algorithm
        self.options = options
        self.blockSize = blockSize
    }
    
    func withUnsafeButes<ResultType>(
        _ body: (_ keyBytes: UnsafeRawBufferPointer,_ ivBytes: UnsafeRawBufferPointer? ) throws -> ResultType
    ) rethrows -> ResultType {
        guard let iv = iv else {
            return try key.data.withUnsafeBytes { bytes in
                return try body(bytes, nil)
            }
        }
        
        return try key.data.withUnsafeBytes { keyBytes in
            return try iv.data.withUnsafeBytes { ivBytes in
                return try body(keyBytes, ivBytes)
            }
        }
    }
}
