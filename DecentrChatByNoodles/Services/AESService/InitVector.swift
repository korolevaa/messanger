//
//  InitVector.swift
//  
//
//  Created by Anton Korolev 
//

import Foundation


class InitVector {
    let data: Data
    let size: Int
    init(data: Data, size: Int) {
        self.data = data
        self.size = size
    }
}
