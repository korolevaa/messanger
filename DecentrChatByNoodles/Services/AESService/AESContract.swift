//
//  AESCrypting.swift
//  
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


/// Протокол расширенного алгоритма шифрования
protocol AESCryptable {
    // MARK: Generate models
    /// произвести симметричный ключ шифрования
    func generateSymmetricKey() -> SymmetricKey?
    func generateInitVector() -> InitVector?

    // MARK: encrypt/decrypt data
    func crypt(
        fromData data: Data,
        usingAES aes: AES
    ) -> Data?
    
    // MARK: encrypt/decrypt file
    func crypt(
        fromSourceURL sourceURL: URL,
        toDestinationURL destinationURL: URL,
        usingAES aes: AES
    ) -> NSURL?
}
