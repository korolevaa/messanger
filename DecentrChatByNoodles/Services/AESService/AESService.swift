//
//  File.swift
//  
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import CommonCrypto


extension AESService {
    enum Result<T> {
        case failure(Error), success(T)
    }
    
    enum AESError: Error {
        case generateRandomData(OSStatus)
        case generateSymetricKey
    }
}


class AESService {
    static let `default` = AESService()
}


// MARK: - Private generate random data
fileprivate extension AESService {
    func generateRandomData(by size: Int) -> Result<Data> {
        var data = Data(count: size)
        let status = data.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, size, $0.baseAddress!)
        }
        switch status {
        case errSecSuccess:
            return .success(data)
        default:
            return .failure(AESError.generateRandomData(status))
        }
    }
}

// MARK: - AESCrypting
extension AESService: AESCryptable {
    /// Реализация создания симетричного ключа шифрования
    func generateSymmetricKey() -> SymmetricKey? {
        switch generateRandomData(by: kCCKeySizeAES128) {
        case .failure:
            return nil
        case let .success(data):
                return SymmetricKey(data: data, size: .aes128)
        }
    }
    
    func generateInitVector() -> InitVector? {
        switch generateRandomData(by: kCCKeySizeAES128) {
        case .failure:
            return nil
        case let .success(data):
            return InitVector(data: data, size: kCCKeySizeAES128)
        }
    }

    // MARK: encrypt/decrypt data
    func crypt(
        fromData data: Data,
        usingAES aes: AES
    ) -> Data? {
        let dataOutSize = data.count + aes.secBlockSize
        var dataOut = Data(count: dataOutSize)
        var dataOutMoved = Int.zero
        
        let status = aes.withUnsafeButes { keyBytes, ivBytes in
            return data.withUnsafeBytes { dataBytes in
                return dataOut.withUnsafeMutableBytes { dataOutBytes in
                    return CCCrypt(
                        aes.secOperation,
                        aes.secAlgoritm,
                        aes.secOptions,
                        keyBytes.baseAddress,
                        aes.key.secSize,
                        ivBytes?.baseAddress,
                        dataBytes.baseAddress,
                        data.count,
                        dataOutBytes.baseAddress,
                        dataOutSize,
                        &dataOutMoved
                    )
                }
            }
        }
        switch Int(status) {
        case kCCSuccess:
            return dataOut[..<dataOutMoved]
        default:
            return .none
        }
    }
    
    // MARK: encrypt/decrypt file
    func crypt(
        fromSourceURL sourceURL: URL,
        toDestinationURL destinationURL: URL,
        usingAES aes: AES
    ) -> NSURL? {
        // TODO: На будущее для файлов
        return nil
    }
}


// MARK: - AES
fileprivate extension AES {
    var secOperation: CCOperation {
        switch operation {
        case .encrypt:
            return CCOperation(kCCEncrypt)
        case .decrypt:
            return CCOperation(kCCDecrypt)
        }
    }
    
    var secAlgoritm: CCAlgorithm {
        switch algorithm {
        case .AES128:
            return CCAlgorithm(kCCAlgorithmAES128)
        case .AES:
            return CCAlgorithm(kCCAlgorithmAES)
        case .DES:
            return CCAlgorithm(kCCAlgorithmDES)
        case .DES3:
            return CCAlgorithm(kCCAlgorithm3DES)
        case .CAST:
            return CCAlgorithm(kCCAlgorithmCAST)
        case .RC4:
            return CCAlgorithm(kCCAlgorithmRC4)
        case .RC2:
            return CCAlgorithm(kCCAlgorithmRC2)
        case .Blowfish:
            return CCAlgorithm(kCCAlgorithmBlowfish)
        }
    }
    
    var secOptions: CCOptions {
        switch options {
        case .PKCS7Padding:
            return CCOptions(kCCOptionPKCS7Padding)
        case .ECBMode:
            return CCOptions(kCCOptionECBMode)
        }
    }
    
    var secBlockSize: Int {
        switch blockSize {
        case .AES128:
            return kCCBlockSizeAES128
        case let .arbitrary(size):
            return size
        }
    }
}


// MARK: - SymmetricKey
fileprivate extension SymmetricKey {
    var secSize: Int {
        switch size {
        case .aes128:
            return kCCKeySizeAES128
        }
    }
}
