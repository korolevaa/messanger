//
//  SymmetricKey.swift
//  
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


class SymmetricKey {
    let data: Data
    let size: Size
    init(data: Data, size: Size) {
        self.data = data
        self.size = size
    }
    
    enum Size: Int, Codable {
        case aes128 // kCCKeySizeAES128
    }
}
