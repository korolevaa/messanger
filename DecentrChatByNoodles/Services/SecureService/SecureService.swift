//
//  SecureService.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import KeychainAccess

fileprivate let publicKeyIdentifier = "com.bittalk.secureservice.publickey"
fileprivate let privateKeyIdentifier = "com.bittalk.secureservice.privatekey"
fileprivate let appKey = "BitTalk Key"


private extension SecureService {
    struct Room: Codable {
        let id: DTO.ID
        let owner: DTO.User
        let createdAt: Double
        let updatedAt: Double
        let cipherData: Data
        let name: String
        let admins: [DTO.User]
        let members: [DTO.User]
        let keys: [DTO.ID: Data]
        let attachments: [DTO.Attachment]
    }
    
    struct Message: Codable {
        let id: DTO.ID
        let owner: DTO.User
        let createdAt: Double
        let updatedAt: Double
        let cipherData: Data
        let roomID: DTO.ID
        let sourceText: String
        let attachments: [DTO.Attachment]
    }
}


class SecureService {
    
    static let `default` = SecureService()
    private let keychain = Keychain(service: "com.bittalk.secureservice.pairkey")
    private lazy var rsaService = RSAService.default
    private lazy var aesService = AESService.default
    private lazy var profile = ProfileService.default
    private lazy var dataBase = DataBase.default
}


// MARK: - BlockChain
fileprivate extension SecureService {
    func symmetricKey(forRoomWithID roomID: DTO.ID) -> SymmetricKey? {
        guard let cipherRoomKey = dataBase.cipherRoomKey(forRoomWithID: roomID),
              let privateKey = userKeyPair?.privateKey
        else {
            return nil
        }
        
        let rsa = RSA(key: .private(privateKey))
        let data = rsaService.decrypt(fromCipherData: cipherRoomKey, usingRSA: rsa)
        return data == nil ? nil : SymmetricKey(data: data!, size: .aes128)
    }
}


// MARK: - KeyChain
fileprivate extension SecureService {
    typealias Attributes = Dictionary<String, AnyObject>
    
    func baseQuery(forKeyClass keyClass: String, appTag: String) -> Attributes  {
        return [
            kSecClass as String : kSecClassKey as AnyObject,
            kSecAttrKeyType as String : kSecAttrKeyTypeRSA as AnyObject,
            kSecAttrKeyClass as String : keyClass as AnyObject,
            kSecAttrApplicationTag as String : appTag.data(using: .utf8) as AnyObject,
        ]
    }
    
    func userVisibleLabel(forKey key: String) -> String {
        switch key as CFString {
        case kSecAttrKeyClassPublic:
            return "Public " + appKey
        case kSecAttrKeyClassPrivate:
            return "Private " + appKey
        case kSecAttrKeyClassSymmetric:
            return "Symmetric " + appKey
        default:
            return appKey
        }
    }
}


// MARK: - SecureAccessible
extension SecureService: SecureAccessible {
    func room(forUsers users: [DTO.User], withName name: String, attachments: [DTO.Attachment]) -> DTO.Room? {
        return room(forUsers: users, withName: name, attachments: attachments, id: UUID().uuidString)
    }
    
    var userKeyPair: KeyPair? {
        get {
            guard   let publicKeyData = try? keychain.getData(publicKeyIdentifier),
                    let privateKeyData = try? keychain.getData(privateKeyIdentifier)
            else {
                return nil
            }
            
            return KeyPair(
                publicKey: publicKeyData,
                privateKey: privateKeyData
            )
        }
        set {
            switch newValue {
            case .none:
                try? keychain.remove(publicKeyIdentifier)
                try? keychain.remove(privateKeyIdentifier)
            case let .some(keyPair):
                try? keychain.set(keyPair.publicKey, key: publicKeyIdentifier)
                try? keychain.set(keyPair.privateKey, key: privateKeyIdentifier)
            }
        }
    }
    
    func message(
        forRoomWithID roomID: DTO.ID,
        withText text: String,
        attachments: [DTO.Attachment]
    ) -> DTO.Message? {
        guard let symmetricKey = symmetricKey(forRoomWithID: roomID),
              let sourceData = text.data(using: .utf8)
        else {
            return nil
        }
        
        let aes = AES(symmetricKey: symmetricKey)
        
        guard   let owner = profile.owner,
                let cipherData = aesService.crypt(fromData: sourceData, usingAES: aes)
        else {
            return nil
        }
        
        let data = Date().timeIntervalSince1970
        
        let message = Message(
            id: UUID().uuidString,
            owner: owner,
            createdAt: data,
            updatedAt: data,
            cipherData: cipherData,
            roomID: roomID,
            sourceText: text,
            attachments: attachments
        )
        
        switch signature(message) {
        case .none:
            return nil
        case let .some(proof):
            return DTO.Message(message: message, signature: proof)
        }
    }
    
    func room(
        forUsers users: [DTO.User],
        withName name: String,
        attachments: [DTO.Attachment] = [],
        id: DTO.ID = UUID().uuidString
    ) -> DTO.Room? {
        guard   let owner = profile.owner,
                let symmetricKey = aesService.generateSymmetricKey() else {
            return nil
        }
        
        let date = Date().timeIntervalSince1970
        let allMembers = [owner] + users
        
        let keys = allMembers.reduce(into: [DTO.ID: Data]()) { keys, user in
            let rsa = RSA(key: .public(user.publicKey))
            if let cipherData = self.rsaService.encrypt(fromData: symmetricKey.data, usingRSA: rsa) {
                keys[user.id] = cipherData
            }
        }
        
        let room = Room(
            id: id,
            owner: owner,
            createdAt: date,
            updatedAt: date,
            // TODO: Я ХЗ что сюда класть.
            cipherData: Data(),
            name: name,
            admins: [owner],
            members: allMembers,
            keys: keys,
            attachments: attachments
        )
        
        switch signature(room) {
        case .none:
            return nil
        case let .some(proof):
            return DTO.Room(room: room, signature: proof)
        }
    }
    
    func signature<T: Codable>(_ object: T) -> Data? {
        guard let ownerPrivateKey = userKeyPair?.privateKey else {
            return nil
        }
        
        guard let sha256 = try? JSONEncoder().encode(object).sha256() else {
            return nil
        }
        
        switch rsaService.createSignature(data: sha256, usingKey: ownerPrivateKey) {
        case .none:
            return nil
        case let .some(proof):
            return proof
        }
    }
    
    func symmetricKey(forRoomKeys keys: [DTO.ID: Data]) -> Data? {
        guard let owner = profile.owner,
              let privateKey = userKeyPair?.privateKey,
              let cipherData = keys[owner.id]
        else {
            return nil
        }
        let rsa = RSA(key: .private(privateKey))
        return rsaService.decrypt(fromCipherData: cipherData, usingRSA: rsa)
    }
}


// MARK: - DTO.Room
fileprivate extension DTO.Room {
    init(room: SecureService.Room, signature: Data) {
        let base = DTO.Base(
            id: room.id,
            owner: room.owner,
            createdAt: room.createdAt,
            updatedAt: room.updatedAt,
            cipherData: room.cipherData
        )
        
        self.init(
            base: base,
            name: room.name,
            admins: room.admins,
            members: room.members,
            attachments: room.attachments,
            keys: room.keys,
            signature: signature)
    }
}


// MARK: - DTO.Message
fileprivate extension DTO.Message {
    init(message: SecureService.Message, signature: Data) {
        let base = DTO.Base(
            id: message.id,
            owner: message.owner,
            createdAt: message.createdAt,
            updatedAt: message.updatedAt,
            cipherData: message.cipherData
        )
        
        self.init(
            base: base,
            roomID: message.roomID,
            sourceText: message.sourceText,
            attachments: message.attachments,
            signature: signature
        )
    }
}
