//
//  SecureContract.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


protocol SecureAccessible {
    var userKeyPair: KeyPair? { get set }
    
    func message(
        forRoomWithID roomID: DTO.ID,
        withText text: String,
        attachments: [DTO.Attachment]
    ) -> DTO.Message?
    
    func room(
        forUsers users: [DTO.User],
        withName name: String,
        attachments: [DTO.Attachment],
        id: DTO.ID
    ) -> DTO.Room?
    
    func room(
        forUsers users: [DTO.User],
        withName name: String,
        attachments: [DTO.Attachment]
    ) -> DTO.Room?
    
    /// Подписываем любой объект
    func signature<T: Codable>(_ object: T) -> Data?
    
    /// Достаем симетричный ключ чата для текущего пользователя
    /// DTO.- ID идентификатор пользователя
    /// Data -Зашифрованный приватным ключем симетричный ключ чата
    /// return Симетричный ключ чата
    func symmetricKey(forRoomKeys keys: [DTO.ID: Data]) -> Data?
}
