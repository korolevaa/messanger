//
//  DTO.Message+Noodles.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 08.10.2021.
//

import Foundation
import MessageKit

extension DTO.Message: MessageType {
    var sender: SenderType {
        return Sender(senderId: self.base.owner.id, displayName: self.base.owner.nick)
    }
    
    var messageId: String {
        return self.base.id
    }
    
    var sentDate: Date {
        return Date(timeIntervalSince1970: self.base.createdAt)
    }
    
    var kind: MessageKind {
        let content = self.sourceText
        if content.containsOnlyEmoji {
            return .emoji(content)
        } else {
            return .text(content)
        }
    }
}

extension Character {
    var isSimpleEmoji: Bool {
        guard let firstScalar = unicodeScalars.first else { return false }
        return firstScalar.properties.isEmoji && firstScalar.value > 0x238C
    }

    var isCombinedIntoEmoji: Bool { unicodeScalars.count > 1 && unicodeScalars.first?.properties.isEmoji ?? false }
    
    var isEmoji: Bool { isSimpleEmoji || isCombinedIntoEmoji }
}

extension String {
    var containsOnlyEmoji: Bool { !isEmpty && !contains { !$0.isEmoji } }
}
