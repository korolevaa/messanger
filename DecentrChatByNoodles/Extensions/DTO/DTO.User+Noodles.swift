//
//  DTOUserExtension.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import Foundation
import MessageKit

extension DTO.User: SenderType {
    var senderId: String {
        return self.id
    }
    
    var displayName: String {
        return self.nick
    }
    
    var nameAvatar: String {
        return randomNameAvatar()
    }
    
    func randomNameAvatar() -> String {
        let namesAvatar = ["heartsEyes", "rainbowEyes", "starsEyes", "tongue", "wink", "XXeyes"]
        return namesAvatar.randomElement()!
    }
}
