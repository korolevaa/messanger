//
//  TorManager.swift
//  DecentrChatByNoodles
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import Foundation
import Tor
import CloudKit
import SwiftUI
import GCDWebServer

import Network
//import CryptoKit

enum TorManagerError: Error {
	case encode
	case other(Error)
}

class TorManager: NSObject {

	private static var _torThread: TorThread?
	
	static let shared = TorManager()
	
	var webServer: GCDWebServer?
	var controller: TorController?
	var responseHandler: ((Data)->[String: Any])?
	
	override init() {
		super.init()
		self.setUpController()
		self.startWebServer()
	}
	
	//URL(string: "http://wm2mpwoqvlpdwnip2tc54ueim46lo2hwnsyvagggak65wgh7v4w2llid.onion/?Test")!
	func request(from: URL, postParams: Data, completion: @escaping ((Result<[String: Any], TorManagerError>)->Void)) {
		self.exec {
			self.controller?.getSessionConfiguration({ config in
				if let config = config {
				let session: URLSession = URLSession(configuration: config)
				var request = URLRequest(url: from)
				request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")  // the request is JSON
				request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")        // the expected response is also JSON
				request.httpMethod = "POST"
				
				request.httpBody = postParams
				
				session.dataTask(with: request) { data, response, error in
					var result: Result<[String: Any], TorManagerError> = .failure(TorManagerError.encode)
					if let data = data,
					   let responseJSON = try? JSONSerialization.jsonObject(with: data, options: []) {
						if let responseJSON = responseJSON as? [String: Any] {
							result = .success(responseJSON)
						}
					}
					completion(result)
				}.resume()
				} else {
					completion(.failure(.encode))
				}
			})
		}
	}
	
	func exec(callback: @escaping ()->Void) {
		if let controller = self.controller {
			controller.authenticate(with: self.cookie, completion: { success, error in
				if error != nil {
					print("Exec Error \(error)")
				}
				controller.addObserver(forCircuitEstablished: { established in
					if !established {
						return
					}
					callback()
				})
			})
		}
		
		print("Cookie: \(String(data: self.cookie, encoding: .utf8)) \(controller)")
		
	}
	
	var hostName: String? {
		var activeHostName: String? = nil
		let hostNameFileUrl = TorManager.configuration_HiddenServiceURL.appendingPathComponent("hostname")
		if let data = try? Data(contentsOf: hostNameFileUrl, options: .mappedIfSafe) {
			activeHostName = String(data: data, encoding: .utf8)
		}
		return activeHostName
	}
}

extension TorManager {
	
	static var configuration_HomeDirectory: String {
		return NSHomeDirectory() + "/tmp"
	}
	
	static var configuration_HiddenServiceURL: URL {
		return URL(fileURLWithPath: self.configuration_HomeDirectory).appendingPathComponent("tor/hiddenService")
	}
	
	static var configuration_TorrcURL: URL {
		return URL(fileURLWithPath: self.configuration_HomeDirectory).appendingPathComponent("tor/.torrc")
	}
	
	static var configuration_ControlSocket: URL {
		return URL(fileURLWithPath: self.configuration_HomeDirectory).appendingPathComponent("tor/cPort")
	}
	static var configuration_DataDirectory: URL {
		return URL(fileURLWithPath: NSTemporaryDirectory())
	}
	
	static func configuration_MakeTorrcFile() {
		let hiddenServiceURL: URL = self.configuration_HiddenServiceURL
		let torrcURL: URL = self.configuration_TorrcURL
		try? FileManager.default.removeItem(at: torrcURL)
		if !((try? FileManager.default.fileExists(atPath: torrcURL.path)) ?? true) {
			
			var torrc = ""
			torrc += "DataDirectory " + URL(fileURLWithPath: NSTemporaryDirectory()).path + "\n"
//			torrc += "HiddenServiceVersion 3\n"
			torrc += "HiddenServiceDir " + hiddenServiceURL.path + "\n"
			torrc += "HiddenServicePort 80 127.0.0.1:8080\n"
//			torrc += "HashedControlPassword 16:746D3691E5FC38D860917F1718B60F80EB529B8B9D404550660697EF2D\n"
			
			try? torrc.write(to: torrcURL, atomically: true, encoding: .utf8)
			let attributes: [FileAttributeKey: AnyObject] = [FileAttributeKey.posixPermissions: NSNumber(value: 0o700)]
			try? FileManager.default.setAttributes(attributes, ofItemAtPath: torrcURL.path)
		}
		
		self.changePrmission(url: hiddenServiceURL)
		
		let url = self.configuration_ControlSocket.deletingLastPathComponent()
		self.changePrmission(url: url)
	}
	
	static let configuration: TorConfiguration = {
		TorManager.configuration_MakeTorrcFile()
		let configuration: TorConfiguration = TorConfiguration();
		configuration.cookieAuthentication = true;
		configuration.dataDirectory = TorManager.configuration_DataDirectory
		configuration.controlSocket = TorManager.configuration_ControlSocket
		configuration.arguments = [
			"--allow-missing-torrc",
			"--ignore-missing-torrc",
			"--defaults-torrc", TorManager.configuration_TorrcURL.path,
			"--SocksPort", "auto",
			"--ControlPort", "auto",
			"--ClientUseIPv6", "1",
//			"--Log", "notice stdout",// debug
		];
		return configuration;
	}()
}

//Controller
extension TorManager {
	func setUpController() {
		self.controller = TorController(socketURL: TorManager.configuration.controlSocket!)
		self.controller?.addObserver(forStatusEvents: { type, severity, action, arguments in
			print("type: \(type) severity: \(severity) action:\(action) arguments:\(arguments)")
			return true
		})
	}
	
	func startController() {
		if !(self.controller?.isConnected ?? true) {
			try? self.controller?.connect()
		}
	}
	
	func stopController() {
		if self.controller?.isConnected ?? false {
			self.controller?.disconnect()
		}
	}
}

//Web Server
extension TorManager {
	func startWebServer() {
		if self.webServer == nil {
			self.webServer = GCDWebServer()
			if let webServer = self.webServer {
				webServer.addDefaultHandler(forMethod: "POST", request: GCDWebServerDataRequest.self) { request in
					if let data = (request as? GCDWebServerDataRequest)?.data {
						let resposeObject: [String: Any] = self.responseHandler?(data) ?? [:]
						return GCDWebServerDataResponse.init(jsonObject: resposeObject, contentType: "application/json")
					}
					return GCDWebServerDataResponse(data: "{}".data(using: .utf8)!, contentType: "application/json")
				}
				webServer.addDefaultHandler(forMethod: "GET", request: GCDWebServerDataRequest.self) { request in
					if let data = (request as? GCDWebServerDataRequest)?.data {
						let resposeObject: [String: Any] = self.responseHandler?(data) ?? [:]
						return GCDWebServerDataResponse.init(jsonObject: resposeObject, contentType: "application/json")
					}
					return GCDWebServerDataResponse(data: "{}".data(using: .utf8)!, contentType: "application/json")
				}
				webServer.start(withPort: 8080, bonjourName: nil)
			}
		}
	}

	func stopWebServer() {
		if let webServer = self.webServer {
			webServer.stop()
			self.webServer = nil
		}
	}
	
}

//Private
extension TorManager {
	private static func changePrmission(url: URL) {
		if !((try? FileManager.default.fileExists(atPath: url.path)) ?? true) {
			let attributes: [FileAttributeKey: AnyObject] = [FileAttributeKey.posixPermissions: NSNumber(value: 0o700)]
			try? FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: attributes)
		} else {
			let attributes: [FileAttributeKey: AnyObject] = [FileAttributeKey.posixPermissions: NSNumber(value: 0o700)]
			try? FileManager.default.setAttributes(attributes, ofItemAtPath: url.path)
		}
	}
	
	private var cookie: Data {
		if let data = try? Data.init(contentsOf: TorManager.configuration.dataDirectory!.appendingPathComponent("control_auth_cookie")) {
			return data
		} else {
			return Data()
		}
	}
}

//Thread
extension TorManager {
	static func setupThread() {
		if self._torThread == nil {
			let thread: TorThread = TorThread(configuration: TorManager.configuration)
			thread.start()
			self._torThread = thread
		}
	}
	
	static func releaseThread() {
		if let thread = self._torThread {
			thread.cancel()
			self._torThread = nil
		}
	}
}

struct TransmitCover: Codable {
	let data: Data
	let type: BlockchainType
	let signature: Data
	let identifier: DTO.ID?
	var pack: Data? {
		return try? JSONEncoder().encode(self)
	}
	static func unpack(_ data: Data) -> TransmitCover? {
		return try? JSONDecoder().decode(TransmitCover.self, from: data)
	}
}
