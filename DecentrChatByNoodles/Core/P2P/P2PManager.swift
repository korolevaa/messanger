//
//  P2PManager.swift
//  DecentrChatByNoodles
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import CoreBluetooth

struct BlockChainStruct {
	var blockCount: Int
	var blockHash: String
	var blockUUID: UUID
	
	init(count: Int, hash: String, uuid: UUID) {
		self.blockCount = count
		self.blockUUID = uuid
		self.blockHash = hash
	}
}



class P2PManager {
	
	var myChartBlockChains: [BlockChainStruct] {
		get {
			return self.periferal?.chartBlockChain ?? []
		}
		set {
			self.periferal?.chartBlockChain = newValue
		}
	}
	
	var myGlobalBlockChain: BlockChainStruct {
		get {
			return self.periferal?.gloabalBlockChain ?? BlockChainStruct(count: 0, hash: "NONE", uuid: UUID())
		}
		set {
			self.periferal?.gloabalBlockChain = newValue
		}
	}
	
	let userID: String
	
	private var periferal: AppPeriferal?
	private var central: AppCentral?
	private var multipeer: MultipeerSession?
	
	fileprivate let serviceUUID = CBUUID(string: "A03C3F0C-0675-11EC-9A03-0242AC130003")
	
	init(userID: String, blockChain: BlockChainStruct) {
		self.userID = userID
		self.periferal = AppPeriferal(
			serviceUUID: serviceUUID,
			blockChain: blockChain,
			userID: self.userID
		)
		self.central = AppCentral(
			serviceUUID: serviceUUID
		)
		self.central?.periferalIsPrepared = { userID, blockChain in
			self.recevedBlockhainStatus?(userID, blockChain)
		}
		self.multipeer = MultipeerSession(userID: self.userID)
		self.multipeer?.recevedData = { data, peer in
			self.recevedData?(peer.displayName, data)
		}
	}
	
	
	var recevedBlockhainStatus: ((_ userID: String,_ blockChain: BlockChainStruct)->Void)?
	var recevedData: ((_ userID: String,_ date: Data)->Void)?
	func send(to userID: String, data: Data) {
		self.multipeer?.send(data: data, to: userID)
	}
}

extension P2PManager {
	private func updateCharacteristic() {

	}
}
