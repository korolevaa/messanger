//
//  PeriferalPoint.swift
//  BLE_Test
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import Foundation
import CoreBluetooth

class PeriferalPoint: NSObject {
	private var inCharacteristicForLoading: [CBService] = []
	private var inServiceForLoading: [CBService] = []
	private var valueCharacteristicForLoading: [CBCharacteristic] = []
	
	private var peripheral: CBPeripheral
	private let serviceUUID: CBUUID
	
	var isLoaded: Bool {
		return inCharacteristicForLoading.isEmpty && inServiceForLoading.isEmpty && valueCharacteristicForLoading.isEmpty
	}
	
	var hasData: Bool {
		return self.userID() != nil && self.globalBlockChain() != nil
	}
	
	var isConnected: Bool {
		return self.peripheral.state == .connected	
	}
	
	init(peripheral: CBPeripheral, uuid: CBUUID) {
		self.serviceUUID = uuid
		self.peripheral = peripheral
		super.init()
		self.peripheral.delegate = self
	}
	
	func connect(cental: CBCentralManager) {
		cental.connect(self.peripheral, options: nil)
	}
	
	func isPeripheral(peripheral: CBPeripheral) -> Bool {
		return self.peripheral == peripheral
	}
	
	
	var hasSomeValue: (()->Void)?
	private func sendUpdateValue() {
		if self.hasData {
			self.hasSomeValue?()
			self.hasSomeValue = nil
		}
	}
}

extension PeriferalPoint: CBPeripheralDelegate {
	func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
		print("PeriferalPoint peripheralDidUpdateName \(peripheral)")
	}

	func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
		print("PeriferalPoint invalidatedServices \(peripheral) invalidatedServices:\(invalidatedServices)")
	}

	
	func peripheralDidUpdateRSSI(_ peripheral: CBPeripheral, error: Error?) {
		print("PeriferalPoint peripheralDidUpdateRSSI \(peripheral) error:\(error)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
		print("PeriferalPoint didReadRSSI \(peripheral) RSSI: \(RSSI) error:\(error)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
		peripheral.services?.forEach({ service in
			
			print("PeriferalPoint service:\(service)")
			self.inServiceForLoading.append(service)
			peripheral.discoverCharacteristics(nil, for: service)
			self.inCharacteristicForLoading.append(service)
			peripheral.discoverIncludedServices(nil, for: service)
		})
		sendUpdateValue()
		print("PeriferalPoint didDiscoverServices \(peripheral) error:\(error)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?) {
		self.inServiceForLoading = self.inServiceForLoading.filter { fservice in
			return fservice != service
		}
		service.includedServices?.forEach({ service in
			print("PeriferalPoint service:\(service)")
			self.inServiceForLoading.append(service)
			peripheral.discoverCharacteristics(nil, for: service)
			self.inCharacteristicForLoading.append(service)
			peripheral.discoverIncludedServices(nil, for: service)
		})
		sendUpdateValue()
		print("PeriferalPoint didDiscoverIncludedServicesFor \(peripheral) service:\(service) error:\(error)")

	}

	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
		print("PeriferalPoint didDiscoverCharacteristicsFor \(peripheral) service.char:\(service.characteristics) error:\(error)")
		self.inCharacteristicForLoading = self.inCharacteristicForLoading.filter { fservice in
			return fservice != service
		}
		service.characteristics?.forEach({ char in
			if char.properties.contains(.read) {
				peripheral.readValue(for: char)
			}
		})
		sendUpdateValue()
	}
 
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
		print("PeriferalPoint didUpdateValueFor \(peripheral) characteristic:\(characteristic) error:\(error)")
		if error == nil {
			if let val = characteristic.value {
				print("PeriferalPoint the value is \(String(bytes:  val, encoding: .utf8))")
			}
		}
		sendUpdateValue()
	}

	
	func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
		print("PeriferalPoint didWriteValueFor \(peripheral) characteristic:\(characteristic) error:\(error)")

	}

	
	func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
		print("PeriferalPoint didUpdateNotificationStateFor \(peripheral) characteristic:\(characteristic) error:\(error)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
		print("PeriferalPoint didDiscoverDescriptorsFor \(peripheral) characteristic:\(characteristic) error:\(error)")
		if let desc = characteristic.descriptors?.first {
			peripheral.readValue(for: desc)
		}
		sendUpdateValue()
	}

	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
		print("PeriferalPoint didUpdateValueFor \(peripheral) descriptor:\(descriptor) error:\(error)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
		print("PeriferalPoint didWriteValueFor \(peripheral) descriptor:\(descriptor) error:\(error)")
	}

	
	func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
		print("PeriferalPoint peripheralIsReady \(peripheral)")
	}

	
	func peripheral(_ peripheral: CBPeripheral, didOpen channel: CBL2CAPChannel?, error: Error?) {
		print("PeriferalPoint didOpen \(peripheral) channel:\(channel) error:\(error)")
	}
}

extension PeriferalPoint {
	
	var mainService: CBService? {
		return self.peripheral.services?.firstService(uuid: self.serviceUUID)
	}
	
	func userID() -> String? {
		let userIDCharacteristic = PeriferalAddress.userIDCharacteristic.uuid
		if let characteristic = self.mainService?.characteristics?.firstСharacteristic(uuid: userIDCharacteristic),
		   let value = characteristic.value,
		   let stringValue = String(data: value, encoding: .utf8) {
			return stringValue
		} else {
			return nil
		}
	}
	
	func globalBlockChain() -> BlockChainStruct? {
		let mainChainServiceUUID = PeriferalAddress.mainChainService.uuid
		if let service = self.mainService?.includedServices?.firstService(uuid: mainChainServiceUUID) {
			func requestCharacteristicsString(uuid: CBUUID) -> String? {
				if let character = service.characteristics?.firstСharacteristic(uuid: uuid),
				   let data = character.value {
					return String.decode(data: data)
				}
				return nil
			}
			func requestCharacteristicsInt(uuid: CBUUID) -> Int? {
				if let uuidCh = service.characteristics?.firstСharacteristic(uuid: uuid),
					let uuidData = uuidCh.value {
					return Int.decode(data: uuidData)
				}
				return nil
			}
			let uuidKey = BlockChainAddress.UUID.uuid
			let hashKey = BlockChainAddress.hash.uuid
			let countKey = BlockChainAddress.count.uuid
			
			let uuidRes: String? = requestCharacteristicsString(uuid: uuidKey)
			let hashRes: String? = requestCharacteristicsString(uuid: hashKey)
			let countRes: Int? = requestCharacteristicsInt(uuid: countKey)
			
			if let uuidRes = uuidRes,
			   let hashRes = hashRes,
			   let countRes = countRes,
			   let uuid = UUID(uuidString: uuidRes) {
				let result = BlockChainStruct(count: countRes, hash: hashRes, uuid: uuid)
				return result
			}
		}
		return nil
	}
	
	
}

extension Array where Element: CBService {
	func firstService(uuid: CBUUID) -> CBService? {
		return self.first(where: { service in
			if service.uuid == uuid {
				return true
			}
			return false
		})
	}
}

extension Array where Element: CBCharacteristic {
	func firstСharacteristic(uuid: CBUUID) -> CBCharacteristic? {
		return self.first(where: { сharacteristic in
			if сharacteristic.uuid == uuid {
				return true
			}
			return false
		})
	}
}

