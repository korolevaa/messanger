//
//  BlockChainIDService.swift
//  DecentrChatByNoodles
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import Foundation
import CoreBluetooth



class BlockChainIDService: CBMutableService {
	let blockChain: BlockChainStruct
	let uuidCharacteristic: ChartCharacteristic<String>
	let hashCharacteristic: ChartCharacteristic<String>
	let countCharacteristic: ChartCharacteristic<Int>

	convenience init(blockChain: BlockChainStruct) {
		self.init(blockChain: blockChain, uuid: nil)
	}
	
	init(blockChain: BlockChainStruct, uuid: CBUUID?) {
		self.blockChain = blockChain
		self.hashCharacteristic = ChartCharacteristic<String>(
			UUID: BlockChainAddress.hash.uuid,
			value: self.blockChain.blockHash,
			permissions: .read
		)
		self.uuidCharacteristic = ChartCharacteristic<String>(
			UUID: BlockChainAddress.UUID.uuid,
			value: self.blockChain.blockUUID.uuidString,
			permissions: .read
		)
		self.countCharacteristic = ChartCharacteristic<Int>(
			UUID: BlockChainAddress.count.uuid,
			value: self.blockChain.blockCount,
			permissions: .read
		)
		super.init(type: uuid ?? CBUUID(nsuuid: blockChain.blockUUID), primary: false)
		self.characteristics = [uuidCharacteristic, hashCharacteristic, countCharacteristic]
	}
}
