//
//  ChartCharacteristic.swift
//  DecentrChatByNoodles
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import CoreBluetooth
import Foundation

class ChartCharacteristic<T: DecodingDataType>: CBMutableCharacteristic {
	
	var preparedValue:T {
		didSet {
			self.value = preparedValue.data
		}
	}
	
	init(
		UUID: CBUUID,
		value: T,
		permissions: Permission) {
			self.preparedValue = value
			super.init(
				type: UUID,
				properties: permissions.properties,
				value: value.data,
				permissions: permissions.permissions
			)
	}
}
 
extension ChartCharacteristic {
	enum Permission {
		case readWirite
		case write
		case read
		
		var properties: CBCharacteristicProperties {
			var resultProperties: CBCharacteristicProperties = []
			switch self {
			case .readWirite:
				resultProperties = [.writeWithoutResponse, .write, .read, .notify]
			case .write:
				resultProperties = [.writeWithoutResponse, .write]
			case .read:
				resultProperties = [.read]
			}
			return resultProperties
		}
		
		var permissions: CBAttributePermissions {
			var resultPermissions: CBAttributePermissions = []
			switch self {
			case .readWirite:
				resultPermissions = [.writeable, .readable]
			case .write:
				resultPermissions = [.writeable]
			case .read:
				resultPermissions = [.readable]
			}
			return resultPermissions
		}
	}
}

protocol DecodingDataType {
	var data: Data { get }
	static func decode(data: Data) -> Self
}

extension String: DecodingDataType {
	static func decode(data: Data) -> String {
		if let result = Self.init(data: data, encoding: .utf8) {
			return result
		}
		return ""
	}
	
	var data: Data {
		return self.data(using: .utf8) ?? Data()
	}
}

extension Int: DecodingDataType {
	static func decode(data: Data) -> Int {
		let value = data.withUnsafeBytes {
			$0.load(as: Int.self).bigEndian
		}
		return value
	}
	
	var data: Data {
		let data = withUnsafeBytes(of: self.bigEndian) { Data($0) }
		return data
	}
}
