//
//  AppPeriferal.swift
//  BLE_Test
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import Foundation
import CoreBluetooth

enum BlockChainAddress: String {
	case hash = "9e7dc04a-278c-11ec-9621-0242ac130002"
	case UUID = "9e7dc266-278c-11ec-9621-0242ac130002"
	case count = "9e7dc568-278c-11ec-9621-0242ac130002"
	var uuid: CBUUID {
		return CBUUID(string: self.rawValue)
	}
}

enum PeriferalAddress: String {
	case mainChainService = "9e7dc888-278c-11ec-9621-0242ac130002"
	case chainListService = "9e7dc9f0-278c-11ec-9621-0242ac130002"
	case userIDCharacteristic = "9e7dc93c-278c-11ec-9621-0242ac130002"
	var uuid: CBUUID {
		return CBUUID(string: self.rawValue)
	}
}

class AppPeriferal: NSObject {
	var chartBlockChain: [BlockChainStruct] = [] {
		didSet {
			self.updateChatServices()
		}
	}
	var gloabalBlockChain: BlockChainStruct
	
	
	private let serviceUUID: CBUUID
	private var mainService: BlockChainIDService
	private var listOfChainsService: CBMutableService
	private var chartServices: [BlockChainIDService] = []
	private var peripheralManager: CBPeripheralManager
	private var peripheralService: CBMutableService
	private var userIDCharacteristic: ChartCharacteristic<String>
	
	init(serviceUUID: CBUUID, blockChain: BlockChainStruct, userID: String) {
		self.serviceUUID = serviceUUID
		self.gloabalBlockChain = blockChain
		//Manager
		self.peripheralManager = CBPeripheralManager(delegate: nil, queue: nil, options: nil)
		
		//Primary Service
		self.peripheralService = CBMutableService(type: self.serviceUUID, primary: true)
		
		//Services
		self.mainService = BlockChainIDService(blockChain: blockChain, uuid: PeriferalAddress.mainChainService.uuid)
		self.listOfChainsService = CBMutableService(type: PeriferalAddress.chainListService.uuid, primary: false)
		
		//Characteristics
		self.userIDCharacteristic = ChartCharacteristic<String>(UUID: PeriferalAddress.userIDCharacteristic.uuid, value: userID, permissions: .read)

		super.init()
		self.peripheralManager.delegate = self
		
	}
	
	func updateState() {
//		self.updateLog?(self.valueString)
	}
	
	func updateChatServices() {
		var services: [CBService] = []
		for chatID in self.chartBlockChain {
			let service = BlockChainIDService(blockChain: chatID)
			services.append(service)
		}
		self.listOfChainsService.includedServices?.removeAll()
		self.listOfChainsService.includedServices = services
	}
	
	func makeServices() {
		updateChatServices()
		self.peripheralService.includedServices = [self.mainService, self.listOfChainsService]
		self.peripheralService.characteristics = [self.userIDCharacteristic]
		self.mainService.resgisterIncludedServicesPeripheralManager(manager: self.peripheralManager)
	}
}

extension AppPeriferal: CBPeripheralManagerDelegate {
	func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
		
		switch(peripheral.state) {
			
		case .unknown:
			print("AppPeriferal unknown \(peripheral)")
		case .resetting:
			print("AppPeriferal resetting \(peripheral)")
		case .unsupported:
			print("AppPeriferal unsupported \(peripheral)")
		case .unauthorized:
			print("AppPeriferal unauthorized \(peripheral)")
		case .poweredOff:
			print("AppPeriferal poweredOff \(peripheral)")
		case .poweredOn:
			print("AppPeriferal poweredOn \(peripheral)")
			if peripheral == self.peripheralManager {
				self.makeServices()
			}
		@unknown default:
			print("AppPeriferal default \(peripheral)")
		}
		self.updateState()
	}
	
	func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String : Any]) {
		print("AppPeriferal willRestoreState \(peripheral) \(dict)")
		self.updateState()
	}

	
	func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
		print("AppPeriferal START \(peripheral) \(error)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
		print("AppPeriferal didAdd \(peripheral) \(error)")
		if service == self.peripheralService {
			peripheral.startAdvertising([CBAdvertisementDataServiceUUIDsKey: [service.uuid]])
		}
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
		print("AppPeriferal didSubscribeTo\(peripheral) \(central) \(characteristic)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFrom characteristic: CBCharacteristic) {
		print("AppPeriferal didUnsubscribeFrom\(peripheral) \(central) \(characteristic)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
		//request.value = valueString.data(using: .utf8)
		peripheral.respond(to: request, withResult: .success)
		print("AppPeriferal didReceiveRead \(peripheral) \(request)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
		print("AppPeriferal didReceiveWrite \(peripheral) \(requests)")
		requests.forEach { request in
			self.peripheralManager.respond(to: request, withResult: .requestNotSupported)
		}
		self.updateState()
	}

	
	func peripheralManagerIsReady(toUpdateSubscribers peripheral: CBPeripheralManager) {
		print("AppPeriferal toUpdateSubscribers \(peripheral)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didPublishL2CAPChannel PSM: CBL2CAPPSM, error: Error?) {
		print("AppPeriferal didPublishL2CAPChannel \(peripheral) \(PSM) \(error)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didUnpublishL2CAPChannel PSM: CBL2CAPPSM, error: Error?) {
		print("AppPeriferal didUnpublishL2CAPChannel \(peripheral) \(PSM) \(error)")
		self.updateState()
	}

	
	func peripheralManager(_ peripheral: CBPeripheralManager, didOpen channel: CBL2CAPChannel?, error: Error?) {
		print("AppPeriferal didOpen \(peripheral) \(channel) \(error)")
		self.updateState()
	}
}


extension CBService {
	func resgisterIncludedServicesPeripheralManager(manager: CBPeripheralManager) {
		if let selfService = self as? CBMutableService {
			selfService.includedServices?.forEach({ service in
				service.resgisterIncludedServicesPeripheralManager(manager: manager)
			})
			manager.add(selfService)
		}
	}
}
