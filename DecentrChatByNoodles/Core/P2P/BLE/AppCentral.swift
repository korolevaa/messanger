//
//  AppCentral.swift
//  BLE_Test
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import Foundation
import CoreBluetooth

class AppCentral: NSObject {
	
	let serviceUUID: CBUUID
	
	var centralManager: CBCentralManager
	
	var periferals: [PeriferalPoint] = []
	var loadedPeriferals: [PeriferalPoint] = []
	
	var periferalIsPrepared:  ((_ userID: String, _ globalBlock: BlockChainStruct)->Void)?
	
	init(serviceUUID: CBUUID) {
		self.serviceUUID = serviceUUID
		self.centralManager = CBCentralManager(delegate: nil, queue: nil, options: nil)
		super.init()
		self.centralManager.delegate = self
	}
	
	
}

extension AppCentral: CBCentralManagerDelegate {
	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		switch central.state {
		case .poweredOn:
			print("AppCentral state poweredOn")
			self.centralManager.scanForPeripherals(withServices: [self.serviceUUID], options: nil)
		case .poweredOff:
			print("AppCentral state poweredOff")
		case .unknown:
			print("AppCentral state unknown")
		case .resetting:
			print("AppCentral state resetting")
		case .unsupported:
			print("AppCentral state unsupported")
		case .unauthorized:
			print("AppCentral state unauthorized")
		@unknown default:
			print("AppCentral state default")
		}
	}

	func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
		print("AppCentral willRestoreState \(dict)")
	}

	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		let peripheral = PeriferalPoint(peripheral: peripheral, uuid: self.serviceUUID)
		peripheral.hasSomeValue = { [weak self] in
			if let self = self,
			   let userID = peripheral.userID(),
			   let globalBlockChain = peripheral.globalBlockChain() {
				self.loadedPeriferals.append(peripheral)
				self.periferalIsPrepared?(userID, globalBlockChain)
			}
			
		}
		periferals.append(peripheral)
		peripheral.connect(cental: self.centralManager)
	}

	
	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		peripheral.discoverServices([self.serviceUUID])
		print("AppCentral didConnect \(peripheral)")
	}

	
	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
		self.periferals.removeAll { point in
			if point.isPeripheral(peripheral: peripheral) {
				return true
			}
			return false
		}
		self.loadedPeriferals.removeAll { point in
			if point.isPeripheral(peripheral: peripheral) {
				return true
			}
			return false
		}
		print("AppCentral didFailToConnect \(peripheral) \(error)")
	}

	
	func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
		self.periferals.removeAll { point in
			if point.isPeripheral(peripheral: peripheral) {
				return true
			}
			return false
		}
		print("AppCentral didDisconnectPeripheral \(peripheral) \(error)")
	}

	
	func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
		print("AppCentral connectionEventDidOccur \(peripheral) \(event)")
	}

	
	func centralManager(_ central: CBCentralManager, didUpdateANCSAuthorizationFor peripheral: CBPeripheral) {
		print("AppCentral didUpdateANCSAuthorizationFor \(peripheral)")
	}
}


