//
//  MultipeerSession.swift
//  BLE_Test
//
//  Created by Anton Zvonarev on 07.10.2021.
//

import MultipeerConnectivity

class MultipeerSession: NSObject, ObservableObject {
	private static let service = "EncChService"
	private let myPeerId: MCPeerID
	private var dataForSending: [(peer: MCPeerID, data: Data)] = []
	
	private var serviceAdvertiser: MCNearbyServiceAdvertiser?
	private let serviceBrowser: MCNearbyServiceBrowser
	private let session: MCSession

	var connectedPeers: [MCPeerID] = []
	var foundedPeers: [MCPeerID] = []
	
	var recevedData: ((_ data: Data,_ peer: MCPeerID) -> Void)?

	init(userID: String) {
		self.myPeerId = MCPeerID(displayName: userID)
		self.session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .none)
		self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: self.myPeerId, discoveryInfo: nil, serviceType: MultipeerSession.service)
		self.serviceBrowser = MCNearbyServiceBrowser(peer: self.myPeerId, serviceType: MultipeerSession.service)

		super.init()

		self.session.delegate = self
		self.serviceAdvertiser?.delegate = self
		self.serviceBrowser.delegate = self

		self.serviceAdvertiser?.startAdvertisingPeer()
		self.serviceBrowser.startBrowsingForPeers()
	}

	deinit {
		self.serviceAdvertiser?.stopAdvertisingPeer()
		self.serviceBrowser.stopBrowsingForPeers()
	}
	
	func send(data: Data, to userId: String) {
		let peer = MCPeerID(displayName: userId)
		self.send(data: data, to: peer)
	}
	
	func send(data: Data, to userPeer: MCPeerID) {
		if self.isFounded(peer: userPeer) {
			if self.isConnected(peer: userPeer) {
				try? self.session.send(data, toPeers: [userPeer], with: .reliable)
			} else {
				self.dataForSending.append((peer: userPeer, data: data))
				self.serviceBrowser.invitePeer(userPeer, to: self.session, withContext: nil, timeout: 10)
			}
		}
	}
	
	func sendAllDataToPeerIfNeeded(peer: MCPeerID) {
		if self.isConnected(peer: peer) {
			self.dataForSending = self.dataForSending.filter({ (fpeer: MCPeerID, fdata: Data) in
				if peer == fpeer {
					self.send(data: fdata, to: peer)
					return false
				}
				return true
			})
		}
	}
	
	func hasDataToSend(peer: MCPeerID) -> Bool {
		if let foundedPeer = self.dataForSending.first(where: { (fpeer: MCPeerID, fdata: Data) in
			if peer == fpeer {
				return true
			}
			return false
		}) {
			return true
		}
		return false
	}
	
	func isFounded(peer: MCPeerID) -> Bool {
		if let foundedPeer = self.foundedPeers.first(where: { userId in
			return peer.displayName == userId.displayName
		}) {
			return true
		}
		return false
	}
	
	func isConnected(peer: MCPeerID) -> Bool {
		if let foundedPeer = self.connectedPeers.first(where: { userId in
			return peer.displayName == userId.displayName
		}) {
			return true
		}
		return false
	}
	
}

extension MultipeerSession: MCNearbyServiceAdvertiserDelegate {
	func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
		print("ServiceAdvertiser didNotStartAdvertisingPeer: \(String(describing: error))")
	}

	func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
		print("didReceiveInvitationFromPeer \(peerID)")
		invitationHandler(true, session)
	}
}

extension MultipeerSession: MCNearbyServiceBrowserDelegate {
	func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
		print("ServiceBrowser didNotStartBrowsingForPeers: \(String(describing: error))")
	}

	func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String: String]?) {
		print("ServiceBrowser found peer: \(peerID)")
		self.foundedPeers.append(peerID)
	}

	func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
		self.foundedPeers = self.foundedPeers.filter { oldPeerID in
			return peerID != oldPeerID
		}
		print("ServiceBrowser lost peer: \(peerID)")
	}
}

extension MultipeerSession: MCSessionDelegate {
	func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
		print("peer \(peerID) didChangeState")
		
		switch state {
			
		case .notConnected:
			self.connectedPeers = self.connectedPeers.filter({ peer in
				return peer != peerID
			})
			if self.hasDataToSend(peer: peerID) {
				self.serviceBrowser.invitePeer(peerID, to: session, withContext: nil, timeout: 10)
			}
			print("notConnected")
		case .connecting:
			print("connecting")
		case .connected:
			print("connected")
			self.connectedPeers.append(peerID)
			if self.hasDataToSend(peer: peerID) {
				self.sendAllDataToPeerIfNeeded(peer: peerID)
			}
		@unknown default:
			print("Unknown")
		}
		
		DispatchQueue.main.async {
			self.connectedPeers = session.connectedPeers
		}

	}

	func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
		print("didReceive bytes \(data.count) bytes")
		recevedData?(data, peerID)
	}

	public func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
		print("Receiving streams is not supported")
	}

	public func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
		print("Receiving resources is not supported")
	}

	public func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
		print("Receiving resources is not supported")
	}
}
