//
//  AppDelegate.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

//	var tes: ColorMultipeerSession = ColorMultipeerSession()

	let tt = P2PManager(
		userID: UUID().uuidString,
		blockChain: BlockChainStruct(count: 0, hash: "Test", uuid: UUID())
	)
	
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
		startTor()
		return true
    }

	private func startTor() {
		TorManager.setupThread()
		
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 10) {
			DataBase.default.setupTorHandlers()
//			TorManager.shared.responseHandler = { request in
//				print("Received: \(request)")
//				return [:]
//			}
			AuthService.default.reacreateUser(addres: "http://"+(TorManager.shared.hostName ?? "")+"/") 
			print("HostNAme: \(TorManager.shared.hostName ?? "")")
			
//			if UIDevice.current.userInterfaceIdiom == .pad {
//				let url: URL = URL(string: "http://kg2daztdy6indylnl7uccuy2nc577dlojvvofyjtacjcam7n5ymbwmyd.onion/?Test")!
//				TorManager.shared.request(from: url, postParams: ["Hello":"Привет мария 🐶"]) { result in
//					print(result)
//
//				}
//			}
		}
	}

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

