//
//  SearchResultsViewController.swift
//  DecentrChatByNoodles
//
//  Created by Maria Bolshakova on 07.10.2021.
//

import Foundation
import SnapKit
import UIKit

protocol SearchResultControllerProtocol: UIViewController {
    var searchString: String? { get set }
}

final class SearchResultsViewController: UIViewController, SearchResultControllerProtocol {
    
    let db = DataBase.default
    
    private let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .green.withAlphaComponent(0.8)
        tableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "ChatTableViewCell")
        tableView.backgroundColor = .black
        return tableView
    }()
    
    enum Section: Int, CaseIterable {
        case romms
        case contacts
    }
    
    var sections: [Section] = []
    var rooms: [DTO.Room] = []
    var contacts: [DTO.User] = []
    
    var searchString: String? {
        didSet {
            rooms = searchString != nil ? db.rooms(forSearchQuery: searchString!) : []
            contacts = searchString != nil ? db.contacts(forSearchQuery: searchString!) : []
            tableView.reloadData()
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true
        
        view.addSubview(tableView)
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
}

extension SearchResultsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = Section(rawValue: section) else {
            return .zero
        }
        switch section {
        case .romms:
            return rooms.count
        case .contacts:
            return contacts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = Section(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        switch section {
        case .romms:
            let room = rooms[indexPath.row]
            cell.configureName(with: room.name)
        case .contacts:
            let contact = contacts[indexPath.row]
                cell.configureName(with: contact.emails.first)
        }
        cell.backgroundColor = .clear
        let colorView = UIView()
        colorView.backgroundColor = .white.withAlphaComponent(0.1)
        cell.selectedBackgroundView = colorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = Section(rawValue: indexPath.section) else {
            return
        }
        
        switch section {
        case .romms:
            let room = rooms[indexPath.row]
            if let searchController = self.parent as? SearchViewController {
                searchController.pushViewControllerWith(room: room)
            }
        case .contacts:
            print("")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
}

