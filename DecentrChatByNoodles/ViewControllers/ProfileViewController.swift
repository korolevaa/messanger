//
//  ProfileViewController.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import UIKit

class ProfileViewController: UIViewController {

    let user: DTO.User
    
    init(user: DTO.User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        print(user, "PROFILE VC 🍎")
        
        super.viewDidLoad()
        self.view.backgroundColor = .black.withAlphaComponent(0.12)
        self.setupSubviews()
        self.avatarImageView.image = UIImage(named: user.nameAvatar)
        self.emailLabel.text = self.user.emails.first
        self.commonName.text = self.user.nick
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2.0
    }
    
    private let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 4.0
        imageView.image = UIImage(named: "smile")
        return imageView
    }()
    
    private let emailLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(25.0)
        label.textColor = .white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let commonName: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(25.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.textColor = .darkGray
        return label
    }()
    
    private func setupSubviews() {
        self.view.addSubview(avatarImageView)
        self.view.addSubview(emailLabel)
        self.view.addSubview(commonName)
        
        avatarImageView.snp.makeConstraints { make in
            make.top.equalTo(100.0)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(80)
        }
        
        emailLabel.snp.makeConstraints { make in
            make.top.equalTo(avatarImageView.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(8.0)
        }
        
        commonName.snp.makeConstraints { make in
            make.top.equalTo(emailLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview().inset(8.0)
        }
    }
}
