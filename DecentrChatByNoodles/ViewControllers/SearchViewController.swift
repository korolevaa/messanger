//
//  SearchViewController.swift
//  DecentrChatByNoodles
//
//  Created by Maria Bolshakova on 07.10.2021.
//

import Foundation
import UIKit

final class SearchViewController: UISearchController {
    private weak var resultsController: SearchResultControllerProtocol?
    
    // MARK: Initializers
    init(searchResultsController: SearchResultControllerProtocol) {
        self.resultsController = searchResultsController
        super.init(searchResultsController: self.resultsController)
        setupSearchController()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSearchController() {
        self.searchBar.searchBarStyle = .minimal
        self.searchBar.barStyle = .black
        self.searchBar.tintColor = .green
        self.searchBar.placeholder = "Поиск чата"
        self.searchBar.sizeToFit()
        self.definesPresentationContext = true
        self.searchResultsUpdater = self
        self.delegate = self
        self.hidesNavigationBarDuringPresentation = false
        self.obscuresBackgroundDuringPresentation = true
        self.showsSearchResultsController = true
        
    }
    
    func pushViewControllerWith(room: DTO.Room) {
        let chatVC = ChatViewController(room: room)
        self.presentingViewController?.navigationController?.pushViewController(chatVC, animated: true)
    }
}

// MARK: - UISearchControllerDelegate
extension SearchViewController: UISearchControllerDelegate {}

// MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        resultsController?.searchString = searchController.searchBar.text
    }
}
