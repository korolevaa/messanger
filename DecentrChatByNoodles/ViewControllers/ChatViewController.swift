//
//  ChatViewController.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import UIKit
import SnapKit
import MessageKit
import InputBarAccessoryView

class ChatMessagesCollectionView: MessagesCollectionView {
    weak var messagesCollectionViewDelegate: MessagesCollectionViewDelegate?
    
    override func handleTapGesture(_ gesture: UIGestureRecognizer) {
        super.handleTapGesture(gesture)
        messagesCollectionViewDelegate?.didTap()
    }
}

class ChatViewController: MessagesViewController {
    private let messageService = MessageService.default
    private let db = DataBase.default
    private var messages: [DTO.Message] = []
    private let user = ProfileService.default.owner!
    private var room: DTO.Room
    
    init(room: DTO.Room) {
        self.room = room
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
    
    override func viewDidLoad() {
        self.messages = db.allMessages(forRoomWithID: room.base.id)
        self.messagesCollectionView = ChatMessagesCollectionView()
        (self.messagesCollectionView as? ChatMessagesCollectionView)?.messagesCollectionViewDelegate = self
        super.viewDidLoad()
        
        if let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout {
            layout.textMessageSizeCalculator.outgoingAvatarSize = .zero
            layout.textMessageSizeCalculator.incomingAvatarSize = .zero
        }
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        self.messagesCollectionView.isScrollEnabled = true
        
        showMessageTimestampOnSwipeLeft = true
        messageInputBar.delegate = self
        
        view.backgroundColor = .black
        self.messagesCollectionView.backgroundColor = .black
    
        self.title = self.room.name
        
        self.configureInputBar()
        self.setupPlusButton()
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: DTO.modifyDataBaseNotificationKey, object: nil)
    }

    @objc private func update() {
        self.room = db.room(withID: room.base.id)!
        self.messages = db.allMessages(forRoomWithID: room.base.id)
        self.messagesCollectionView.reloadData()
    }
    
    private func setupPlusButton() {
        let addButton = UIBarButtonItem(image: UIImage(systemName: "plus.circle"), style: .plain, target: self, action: #selector(addContacts))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc private func addContacts() {
        let vc = ContactListViewController(room: room)
        self.navigationController?.show(vc, sender: self)
    }
    
    func insertMessage(_ message: String) {
        messageService.create(messageWithText: message, toRoomWithID: room.base.id) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .failure:
                return
            case let .sucsess(message):
                self.messages.append(message)
                self.messagesCollectionView.reloadData()
            }
        }
    }
}

extension ChatViewController: MessagesDataSource {
    
    func currentSender() -> SenderType { //own messages
//        let user = SampleData.shared.me
        return Sender(senderId: self.user.id, displayName: self.user.nick)
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType { //конретное сообщение из всего массива
        return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}

extension ChatViewController: MessagesLayoutDelegate {}

extension ChatViewController: MessagesDisplayDelegate {
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .green.withAlphaComponent(0.6) : .systemBlue.withAlphaComponent(0.6)
        
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .black
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        //        if isFromCurrentSender(message: message) {
        //            avatarView.set(avatar: Avatar(image: SampleData.shared.me.profilePic))
        //        }
        //        guard let avatar = SampleData.shared.getAvatarFor(sender: message.sender) else { return }
        //        avatarView.set(avatar: avatar)
        avatarView.isHidden = true        
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        return .bubble
    }
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return .zero
    }
}

extension ChatViewController: InputBarAccessoryViewDelegate {
    
    @objc func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        processInputBar(messageInputBar)
    }
    
    func processInputBar(_ inputBar: InputBarAccessoryView) {
        
        let components = inputBar.inputTextView.components
        
        inputBar.inputTextView.text = String()
        inputBar.sendButton.startAnimating()
        inputBar.inputTextView.placeholder = "Sending..."
        
        DispatchQueue.global(qos: .default).async {
            DispatchQueue.main.async { [weak self] in
                inputBar.sendButton.stopAnimating()
                inputBar.inputTextView.placeholder = "Aa"
                self?.insertMessages(components)
                self?.messagesCollectionView.scrollToLastItem(animated: true)
            }
        }
    }
    
    //TODO: чтоб сообщения опраялись на сервер + доабялись в колекшион
    private func insertMessages(_ data: [Any]) {
        for component in data {
            if let str = component as? String {
                insertMessage(str)
                print("🔴 nedd add messaged to collection view \(str) 🔴")
            }
        }
    }
}

extension ChatViewController {
    func configureInputBar() {
        messageInputBar.separatorLine.isHidden = true
        messageInputBar.backgroundView.backgroundColor = .black
        messageInputBar.inputTextView.backgroundColor = .black
        messageInputBar.inputTextView.placeholderTextColor = .white
        messageInputBar.isTranslucent = false
        messageInputBar.inputTextView.textColor = .white

        messageInputBar.inputTextView.layer.cornerRadius = 20.0
        messageInputBar.inputTextView.layer.borderWidth = 0.3
        messageInputBar.inputTextView.layer.borderColor = UIColor(red:255/255, green:255/255, blue:255/255, alpha: 0.4).cgColor

        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 14, left: 20, bottom: 14, right: 30)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 14, left: 25, bottom: 10, right: 30)

        messageInputBar.sendButton.title = "SEND"
        messageInputBar.sendButton.setTitleColor(.green, for: .normal)
        messageInputBar.sendButton.setSize(CGSize(width: 370.0, height: 50.0), animated: false)
    }
}

protocol MessagesCollectionViewDelegate: AnyObject {
    func didTap()
}

extension ChatViewController: MessagesCollectionViewDelegate {
    func didTap() {
        self.messageInputBar.inputTextView.resignFirstResponder()
    }
}
