//
//  ContactListViewController.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 08.10.2021.
//

import UIKit

class ContactListViewController: UIViewController {
    let db = DataBase.default
    let room: DTO.Room
    
    var users: [DTO.User] = []
    var candidats: [DTO.User] = []
    var tableView: UITableView = {
        let tableView = UITableView.init(frame: .zero, style: .plain)
        tableView.separatorStyle = .singleLine
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.separatorColor = .green.withAlphaComponent(0.8)
        tableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "ChatTableViewCell")
        tableView.backgroundColor = .black.withAlphaComponent(0.12)
        tableView.backgroundView = UIView()
        return tableView
    }()
    
    init(room: DTO.Room) {
        self.room = room
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        users = db.allContacts()
        candidats = room.members
        self.extendedLayoutIncludesOpaqueBars = true
        self.tableView.dataSource = self
        self.tableView.delegate = self
        setupSubviews()
        setupPlusButton()
    }
    
    private func setupSubviews() {
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
    
    
    private func setupPlusButton() {
        let addButton = UIBarButtonItem(image: UIImage(systemName: "plus.circle"), style: .plain, target: self, action: #selector(addContacts))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc private func addContacts() {
        MessageService.default.update(candidats, forRoomWithID: room.base.id) { [weak self] result in
            DispatchQueue.main.async {
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension ContactListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = users[indexPath.row]
        let cell = UITableViewCell.init(style: .subtitle, reuseIdentifier: "asd")
        cell.textLabel?.text = user.emails.first
        cell.textLabel?.textColor = .white
        if candidats.contains(where: { $0.id == user.id }) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.backgroundColor = .clear
        let colorView = UIView()
        colorView.backgroundColor = .white.withAlphaComponent(0.1)
        cell.selectedBackgroundView = colorView
        return cell
    }
}

extension ContactListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let user = users[indexPath.row]
        if candidats.contains(where: { $0.id == user.id }) {
            candidats.removeAll { $0.id == user.id }
        } else {
            candidats.append(user)
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

