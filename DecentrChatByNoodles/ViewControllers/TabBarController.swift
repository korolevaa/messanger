//
//  TabBarController.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import UIKit

class TabBarController: UITabBarController {

    private let user: DTO.User
    private var searchController: SearchViewController = SearchViewController(searchResultsController: SearchResultsViewController())
    private let userIconSelected: UIImage = {
       let icon = UIImage(named: "userIcon")
        return icon!
    }()
    
    private let chatIconSelected: UIImage = {
       let icon = UIImage(named: "chatIcon")
        return icon!
    }()
    
    private let userIcon: UIImage = {
       let icon = UIImage(named: "userIconNotSelected")
        return icon!
    }()
    
    private let chatIcon: UIImage = {
       let icon = UIImage(named: "chatIconNotSelected")
        return icon!
    }()
    
    init(user: DTO.User) {
        self.user = user
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black.withAlphaComponent(0.12)
        UITabBar.appearance().barTintColor = .black.withAlphaComponent(0.12)
        tabBar.tintColor = .green
        setupVCs()
    }
    
    private func setupVCs() {
        viewControllers = [
            createNavController(for: ChatListViewController(searchController: searchController),
                                   title: NSLocalizedString("Чаты", comment: ""),
                                   image: chatIcon,
                                   selectedImage: chatIconSelected),
            createNavController(for: ProfileViewController(user: user),
                                   title: NSLocalizedString("Профиль", comment: ""),
                                   image: userIcon,
                                   selectedImage: userIconSelected)
        ]
    }

    private func createNavController(for rootViewController: UIViewController,
                                     title: String,
                                     image: UIImage,
                                     selectedImage: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.tabBarItem.image = image
        navController.navigationBar.tintColor = .green
        navController.tabBarItem.selectedImage = selectedImage
        navController.tabBarItem.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        navController.navigationBar.isTranslucent = false
        navController.navigationBar.prefersLargeTitles = false
        navController.navigationBar.barStyle = .black
        rootViewController.navigationItem.title = title
        return navController
    }

}
