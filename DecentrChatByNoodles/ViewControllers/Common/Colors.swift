//
//  Colors.swift
//  DecentrChatByNoodles
//
//  Created by Maria Bolshakova on 07.10.2021.
//

import Foundation
import UIKit

enum Colors {
    static let darkGrey: UIColor = UIColor(red: 16, green: 14, blue: 14, alpha: 0)
}
