//
//  ChatListViewController.swift
//  DecentrChatByNoodles
//
//  Created by Kristina Rudakova on 07.10.2021.
//

import UIKit
import SnapKit

class ChatListViewController: UIViewController {
    
    private let db = DataBase.default
    private var messageService = MessageService.default
    private var searchController: SearchViewController
    
    private let chatsTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.separatorStyle = .singleLine
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.separatorColor = .green.withAlphaComponent(0.8)
        tableView.register(ChatTableViewCell.self, forCellReuseIdentifier: "ChatTableViewCell")
        tableView.backgroundColor = .black.withAlphaComponent(0.12)
        tableView.backgroundView = UIView()
        return tableView
    }()
        
    private let nameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.numberOfLines = 1
        nameLabel.textColor = UIColor.white
        return nameLabel
    }()
    
    
    var rooms: [DTO.Room] = []
    
    init(searchController: SearchViewController) {
        self.searchController = searchController
        super.init(nibName: nil, bundle: nil)
        
        setSearchController()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.rooms = db.allRooms()
        self.extendedLayoutIncludesOpaqueBars = true
        self.chatsTableView.dataSource = self
        self.chatsTableView.delegate = self
    
        setupSubviews()
        setupPlusButton()
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: DTO.modifyDataBaseNotificationKey, object: nil)
    }

    @objc private func update() {
        self.rooms = db.allRooms()
        chatsTableView.reloadData()
    }
    
    private func setupSubviews() {
        self.view.addSubview(chatsTableView)
        
        chatsTableView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
    }
    
    private func setSearchController() {
        //TODO: строка поиска появляется только если потянуть за таблицу
        self.navigationItem.searchController = searchController
    }
    
    private func setupPlusButton() {
        let addButton = UIBarButtonItem(image: UIImage(systemName: "plus.circle"), style: .plain, target: self, action: #selector(createRoom))
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc private func createRoom() {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertController.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = .black.withAlphaComponent(0.85)
        alertController.view.tintColor = .green
        let attributedString = NSAttributedString(string: "Придумайте название чата",
                                                  attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        alertController.setValue(attributedString, forKey: "attributedTitle")
        let createAction = UIAlertAction(title: "Добавить", style: .default) { [weak self, weak alertController] action in
            guard let name = alertController?.textFields?.first?.text, !name.isEmpty else {
                return
            }
            self?.messageService.create(forUsers: [], roomWithName: name, []) { [weak self] result in
                print(result, "🍆")
                if let rooms = self?.db.allRooms() {
                    self?.rooms = rooms
                    self?.chatsTableView.reloadData()
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel)
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(createAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension ChatListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
        let room = rooms[indexPath.row]
        cell.configureName(with: room.name)
        cell.backgroundColor = .clear
        let colorView = UIView()
        colorView.backgroundColor = .white.withAlphaComponent(0.1)
        cell.selectedBackgroundView = colorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = self.rooms[indexPath.row]
        let chatVC = ChatViewController(room: room)
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
