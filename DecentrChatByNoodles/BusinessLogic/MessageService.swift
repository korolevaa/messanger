//
//  MessageService.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation
import MapKit


private extension MessageService {
    struct Transaction: Codable {
        let id: UInt64
        let owner: String
        let kind: Int
        let data: Data
    }
}


class MessageService {
    enum MessageError: Error {
        case roomNotEncode
        case roomNotFound
        case messageNotEncode
        case transactionNotSign
    }
    
    static let `default`: MessageService = {
        return MessageService(
            secureService: SecureService.default,
            profileService: ProfileService.default
        )
    }()
    
    typealias ResultBlock<S> = DTO.ResultBlock<S, MessageError>
    typealias RoomID = DTO.ID
    
    private let profileService: ProfileInformable
    
    private let secureService: SecureAccessible
    
    /// Глобальный блокчейн с информацией о пользователях и чатов
    private var globalBlockchain: Blockchain {
        return DataBase.default.globalBlockchain
    }
    
    /// Блокчейны чатов в которых состоит пользователь
    private var inviteRooms: [RoomID: Blockchain] {
        return DataBase.default.inviteRooms
    }
    
    /// Кладовка, блокчейны чатов в которых пользователь не состоит, но по запросу может передать.
    private var pantryRooms: [RoomID: Blockchain] {
        return DataBase.default.pantryRooms
    }
    
    private let queue = DispatchQueue(
        label: "ru.sberhack.bittalk.messageservice.queue",
        qos: .default)
    
    init(secureService: SecureAccessible,
         profileService: ProfileInformable) {
        self.secureService = secureService
        self.profileService = profileService
    }
}

extension MessageService {
    func create(
        messageWithText text: String,
        toRoomWithID roomID: DTO.ID,
        _ completion: ResultBlock<DTO.Message>? = nil
    ) {
        queue.async { [weak self] in
            guard let self = self else { return }
            
            guard let room = self.inviteRooms[roomID] else {
                completion?(.failure(.roomNotFound))
                return
            }
         
            let message = self.secureService.message(
                forRoomWithID: roomID,
                withText: text,
                attachments: [])
            
            guard   let message = message,
                    let data = try? JSONEncoder().encode(message),
                    let owner = self.profileService.owner
            else {
                completion?(.failure(.messageNotEncode))
                return
            }
            
            let id = room.lastTransaction?.id
            let transaction = Transaction(
                id: id == nil ? .zero : id! + 1,
                owner: owner.id,
                kind: DTO.Kind.message.rawValue,
                data: data )

            guard let proof = self.secureService.signature(transaction) else {
                completion? (.failure(.transactionNotSign))
                return
            }
            
            let signTransaction = BlockTransaction(
                transaction: transaction,
                signature: proof)
            
            room.appendTransaction(signTransaction)
            room.mine()
            DataBase.default.safeContext()
            
            DispatchQueue.main.async {
                completion?(.sucsess(message))
            }
        }
    }
    
    func create(
        forUsers users: [DTO.User],
        roomWithName name: String,
        _ attachments: [DTO.Attachment],
        _ completion: ResultBlock<DTO.Room>? = nil
    ) {
        queue.async { [weak self] in
            guard let self = self else { return }
            
            guard let room = self.secureService.room(forUsers: users, withName: name, attachments: attachments),
                  let data = try? JSONEncoder().encode(room),
                  let owner = self.profileService.owner
            else {
                completion? (.failure(.roomNotEncode))
                return
            }
            
            
            let id = self.globalBlockchain.lastTransaction?.id
            let transaction = Transaction(
                id: id == nil ? .zero : id! + 1,
                owner: owner.id,
                kind: DTO.Kind.room.rawValue,
                data: data )
            
            guard let proof = self.secureService.signature(transaction) else {
                completion? (.failure(.transactionNotSign))
                return
            }
            
            let signTransaction = BlockTransaction(
                transaction: transaction,
                signature: proof)
            
            self.globalBlockchain.appendTransaction(signTransaction)
            self.globalBlockchain.mine()
            DataBase.default.inviteRooms[room.base.id] = Blockchain()
			DataBase.default.setupTorHandlers()
            DataBase.default.safeContext()
            
            DispatchQueue.main.async {
                completion? (.sucsess(room))
            }
        }
    }
    
    func update(_ users: [DTO.User], forRoomWithID roomID: DTO.ID, _ completion: ResultBlock<DTO.Room>? = nil) {
        queue.async { [weak self] in
            guard let self = self else { return }
            guard let previousRoom = DataBase.default.room(withID: roomID) else { return }
            
            guard let room = self.secureService.room(forUsers: users, withName: previousRoom.name, attachments: previousRoom.attachments, id: previousRoom.base.id),
                  let data = try? JSONEncoder().encode(room),
                  let owner = self.profileService.owner
            else {
                completion? (.failure(.roomNotEncode))
                return
            }
            
            
            let id = self.globalBlockchain.lastTransaction?.id
            let transaction = Transaction(
                id: id == nil ? .zero : id! + 1,
                owner: owner.id,
                kind: DTO.Kind.room.rawValue,
                data: data )
            
            guard let proof = self.secureService.signature(transaction) else {
                completion? (.failure(.transactionNotSign))
                return
            }
            
            let signTransaction = BlockTransaction(
                transaction: transaction,
                signature: proof)
            
            self.globalBlockchain.appendTransaction(signTransaction)
            self.globalBlockchain.mine()
            DataBase.default.safeContext()
            
            DispatchQueue.main.async {
                completion? (.sucsess(room))
            }
        }
    }
}


// MARK: - Transaction
fileprivate extension BlockTransaction {
    init(transaction: MessageService.Transaction, signature: Data) {
        self.init(
            id: transaction.id,
            owner: transaction.owner,
            kind: transaction.kind,
            data: transaction.data,
            signature: signature
        )
    }
}
