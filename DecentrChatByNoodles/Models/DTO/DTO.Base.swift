//
//  DTO.Base.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct Base: Codable {
        let id: DTO.ID
        let owner: DTO.User
        let createdAt: Double
        let updatedAt: Double
        let cipherData: Data
    }
}

