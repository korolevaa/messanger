//
//  DTO.Room.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct Room: Codable {
        let base: DTO.Base
        let name: String
        let admins: [DTO.User]
        let members: [DTO.User]
        let attachments: [DTO.Attachment]
        
        // Ключи пользователей [ID пользователя: зашифрованный публичным ключом ключ]
        let keys: [DTO.ID: Data]
        
        let signature: Data
    }
}
