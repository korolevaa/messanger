//
//  DTO.Attachment.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct Attachment: Codable {
        let base: DTO.Base
        let size: UInt64
        let mime: String
        let name: String
    }
}
