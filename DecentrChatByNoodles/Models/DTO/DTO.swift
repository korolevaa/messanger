//
//  File.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation

struct DTO {
//   typealias ID = UInt64
    typealias ID = String
    
    typealias EmptyBlock = () -> Void
    typealias ResultBlock<S, E> = (DTO.Result<S, E>) -> Void
    
    static let modifyDataBaseNotificationKey = Notification.Name("com.bittalk.dto.notification.update")
    
    enum Kind: Int, Codable {
		case message = 0
		case room = 1
		case contact = 2
    }
}
