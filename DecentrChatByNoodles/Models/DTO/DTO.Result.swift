//
//  DTO.Result.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    enum Result<S, E> {
        case sucsess(S)
        case failure(E)
    }
}
