//
//  DTO.Transaction.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct Transaction {
        let base: DTO.Base
        let ownerID: DTO.ID
        let messageID: DTO.ID
        let roomID: DTO.ID
    }
}
