//
//  DTO.Message.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct Message: Codable {
        let base: DTO.Base
        let roomID: DTO.ID
        let sourceText: String
        let attachments: [DTO.Attachment]
        let signature: Data
    }
}
