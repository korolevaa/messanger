//
//  DTO.User.swift
//  DecentrChatByNoodles
//
//  Created by Anton Korolev on 07.10.2021.
//

import Foundation


extension DTO {
    struct User: Codable {
        /// Берем из сертификата common name
        let id: String
        
        /// Дата создания
        let createdAt: Double
        
        /// Данные сертификата
        let emails: [String]
        
        /// При регистрации подставляется common name из серта, или пользователь сам пишет, в любом случае может позже сменить.
        let nick: String
        
        /// Публичный ключ пользователя.
        let publicKey: Data
        
        /// Адрес для подключения
        let addres: String
        
        /// Подтверждение
        let signature: Data
    }
}
